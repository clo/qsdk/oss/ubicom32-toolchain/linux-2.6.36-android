/*
 * drivers/net/ubi32_na.c
 *	Ubicom32 Network Accelerator driver.
 *
 * (C) Copyright 2010, 2011, Ubicom, Inc.
 *
 * This file is part of the Ubicom32 Linux Kernel Port.
 *
 * The Ubicom32 Linux Kernel Port is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Ubicom32 Linux Kernel Port is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Ubicom32 Linux Kernel Port.  If not,
 * see <http://www.gnu.org/licenses/>.
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/sysdev.h>
#include <linux/interrupt.h>

#include <linux/in.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/mii.h>
#include <linux/if_vlan.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/skbuff.h>
#include <net/arp.h>
#include <net/ndisc.h>
#include <asm/devtree.h>
#include "../../net/bridge/br_private.h"

#include <net/ubi32_na.h>

/*
 * The NA IPv4 rule creation structure.
 */
struct na_ipv4_rule_create {
	uint8_t protocol;		/* Protocol number */
	int32_t src_interface_num;	/* Source interface number */
	int32_t dest_interface_num;	/* Destination interface number */
	uint8_t flags;			/* Bit flags associated with the rule */
	uint32_t src_ip;		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip;		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	uint16_t src_mac[3];		/* Source MAC address */
	uint16_t dest_mac[3];		/* Destination MAC address */
	uint32_t src_ip_xlate;		/* Translated source IP address */
	uint32_t src_ident_xlate;	/* Translated source ident (e.g. port) */
	uint32_t dest_ip_xlate;		/* Translated destination IP address */
	uint32_t dest_ident_xlate;	/* Translated destination ident (e.g. port) */
	uint16_t src_mac_xlate[3];	/* Translated source MAC address */
	uint16_t dest_mac_xlate[3];	/* Translated destination MAC address */
	uint8_t flow_window_scale;	/* Flow direction's window scaling factor */
	uint32_t flow_max_window;	/* Flow direction's largest seen window */
	uint32_t flow_end;		/* Flow direction's largest seen sequence + segment length */
	uint32_t flow_max_end;		/* Flow direction's largest seen ack + max(1, win) */
	uint8_t return_window_scale;	/* Return direction's window scaling factor */
	uint32_t return_max_window;	/* Return direction's largest seen window */
	uint32_t return_end;		/* Return direction's largest seen sequence + segment length */
	uint32_t return_max_end;	/* Return direction's largest seen ack + max(1, win) */
};

/*
 * NA IPv4 rule creation flags.
 */
#define NA_IPV4_RULE_CREATE_FLAG_NO_SEQ_CHECK 0x01
					/* Do not perform sequence number checks */

/*
 * The NA IPv4 rule destruction structure.
 */
struct na_ipv4_rule_destroy {
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip;		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip;		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
};

/*
 * The NA IPv6 rule creation structure.
 */
struct na_ipv6_rule_create {
	uint8_t protocol;		/* Protocol number */
	int32_t src_interface_num;	/* Source interface number */
	int32_t dest_interface_num;	/* Destination interface number */
	uint8_t flags;			/* Bit flags associated with the rule */
	uint32_t src_ip[4];		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip[4];		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	uint16_t src_mac[3];		/* Source MAC address */
	uint16_t dest_mac[3];		/* Destination MAC address */
	uint8_t flow_window_scale;	/* Flow direction's window scaling factor */
	uint32_t flow_max_window;	/* Flow direction's largest seen window */
	uint32_t flow_end;		/* Flow direction's largest seen sequence + segment length */
	uint32_t flow_max_end;		/* Flow direction's largest seen ack + max(1, win) */
	uint8_t return_window_scale;	/* Return direction's window scaling factor */
	uint32_t return_max_window;	/* Return direction's largest seen window */
	uint32_t return_end;		/* Return direction's largest seen sequence + segment length */
	uint32_t return_max_end;	/* Return direction's largest seen ack + max(1, win) */
};

/*
 * NA IPv6 rule creation flags.
 */
#define NA_IPV6_RULE_CREATE_FLAG_NO_SEQ_CHECK 0x01
					/* Do not perform sequence number checks */

/*
 * The NA IPv6 rule destruction structure.
 */
struct na_ipv6_rule_destroy {
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip[4];		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip[4];		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
};

/*
 * The NA MAC address structure.
 */
struct na_mac_address_set {
	uint8_t mac_addr[6];		/* MAC address */
	int32_t interface;		/* Interface for which MAC address will be set */
};

/*
 * Types of TX metadata.
 */
enum na_tx_metadata_types {
	NA_TX_METADATA_TYPE_IPV4_RULE_CREATE,
	NA_TX_METADATA_TYPE_IPV4_RULE_DESTROY,
	NA_TX_METADATA_TYPE_IPV6_RULE_CREATE,
	NA_TX_METADATA_TYPE_IPV6_RULE_DESTROY,
	NA_TX_METADATA_TYPE_MAC_ADDR_SET,
	NA_TX_METADATA_TYPE_DESTROY_ALL_RULES
};

/*
 * Exeption events from PE
 */
enum exception_events_unknown {
	NA_EXCEPTION_EVENT_UNKNOWN_L2_PROTOCOL,
	NA_EXCEPTION_EVENT_UNKNOWN_LAST
};

/*
 * Exeption events from PE
 */
enum exception_events_ipv4 {
	NA_EXCEPTION_EVENT_IPV4_ICMP_HEADER_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV4_ICMP_UNHANDLED_TYPE,
	NA_EXCEPTION_EVENT_IPV4_ICMP_IP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV4_ICMP_UDP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV4_ICMP_TCP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV4_ICMP_UNKNOWN_PROTOCOL,
	NA_EXCEPTION_EVENT_IPV4_ICMP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV4_ICMP_FLUSH_TO_HOST,
	NA_EXCEPTION_EVENT_IPV4_TCP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV4_TCP_IP_OPTION,
	NA_EXCEPTION_EVENT_IPV4_TCP_IP_FRAGMENT,
	NA_EXCEPTION_EVENT_IPV4_TCP_SMALL_TTL,
	NA_EXCEPTION_EVENT_IPV4_TCP_NEEDS_FRAGMENTATION,
	NA_EXCEPTION_EVENT_IPV4_TCP_FLAGS,
	NA_EXCEPTION_EVENT_IPV4_TCP_SEQ_EXCEEDS_RIGHT_EDGE,
	NA_EXCEPTION_EVENT_IPV4_TCP_SMALL_DATA_OFFS,
	NA_EXCEPTION_EVENT_IPV4_TCP_BAD_SACK,
	NA_EXCEPTION_EVENT_IPV4_TCP_BIG_DATA_OFFS,
	NA_EXCEPTION_EVENT_IPV4_TCP_SEQ_BEFORE_LEFT_EDGE,
	NA_EXCEPTION_EVENT_IPV4_TCP_ACK_EXCEEDS_RIGHT_EDGE,
	NA_EXCEPTION_EVENT_IPV4_TCP_ACK_BEFORE_LEFT_EDGE,
	NA_EXCEPTION_EVENT_IPV4_UDP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV4_UDP_IP_OPTION,
	NA_EXCEPTION_EVENT_IPV4_UDP_IP_FRAGMENT,
	NA_EXCEPTION_EVENT_IPV4_UDP_SMALL_TTL,
	NA_EXCEPTION_EVENT_IPV4_UDP_NEEDS_FRAGMENTATION,
	NA_EXCEPTION_EVENT_IPV4_WRONG_TRAGET_MAC,
	NA_EXCEPTION_EVENT_IPV4_IP_FRAGMENT,
	NA_EXCEPTION_EVENT_IPV4_UNKNOWN_PROTOCOL,
	NA_EXCEPTION_EVENT_IPV4_LAST,
};

/*
 * Exeption events from PE
 */
enum exception_events_ipv6 {
	NA_EXCEPTION_EVENT_IPV6_ICMP_HEADER_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV6_ICMP_UNHANDLED_TYPE,
	NA_EXCEPTION_EVENT_IPV6_ICMP_IP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV6_ICMP_UDP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV6_ICMP_TCP_NOT_FIT,
	NA_EXCEPTION_EVENT_IPV6_ICMP_UNKNOWN_PROTOCOL,
	NA_EXCEPTION_EVENT_IPV6_ICMP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV6_ICMP_FLUSH_TO_HOST,
	NA_EXCEPTION_EVENT_IPV6_TCP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV6_TCP_SMALL_HOP_LIMIT,
	NA_EXCEPTION_EVENT_IPV6_TCP_NEEDS_FRAGMENTATION,
	NA_EXCEPTION_EVENT_IPV6_TCP_FLAGS,
	NA_EXCEPTION_EVENT_IPV6_TCP_SEQ_EXCEEDS_RIGHT_EDGE,
	NA_EXCEPTION_EVENT_IPV6_TCP_SMALL_DATA_OFFS,
	NA_EXCEPTION_EVENT_IPV6_TCP_BAD_SACK,
	NA_EXCEPTION_EVENT_IPV6_TCP_BIG_DATA_OFFS,
	NA_EXCEPTION_EVENT_IPV6_TCP_SEQ_BEFORE_LEFT_EDGE,
	NA_EXCEPTION_EVENT_IPV6_TCP_ACK_EXCEEDS_RIGHT_EDGE,
	NA_EXCEPTION_EVENT_IPV6_TCP_ACK_BEFORE_LEFT_EDGE,
	NA_EXCEPTION_EVENT_IPV6_UDP_NO_ICME,
	NA_EXCEPTION_EVENT_IPV6_UDP_SMALL_HOP_LIMIT,
	NA_EXCEPTION_EVENT_IPV6_UDP_NEEDS_FRAGMENTATION,
	NA_EXCEPTION_EVENT_IPV6_WRONG_TRAGET_MAC,
	NA_EXCEPTION_EVENT_IPV6_UNKNOWN_PROTOCOL,
	NA_EXCEPTION_EVENT_IPV6_LAST,
};

static char *exception_events_unknown_string[NA_EXCEPTION_EVENT_UNKNOWN_LAST] = {
	"UNKNOWN_L2_PROTOCOL"
};

static char *exception_events_ipv4_string[NA_EXCEPTION_EVENT_IPV4_LAST] = {
	"IPV4_ICMP_HEADER_NOT_FIT",
	"IPV4_ICMP_UNHANDLED_TYPE",
	"IPV4_ICMP_IP_NOT_FIT",
	"IPV4_ICMP_UDP_NOT_FIT",
	"IPV4_ICMP_TCP_NOT_FIT",
	"IPV4_ICMP_UNKNOWN_PROTOCOL",
	"IPV4_ICMP_NO_ICME",
	"IPV4_ICMP_FLUSH_TO_HOST",
	"IPV4_TCP_NO_ICME",
	"IPV4_TCP_IP_OPTION",
	"IPV4_TCP_IP_FRAGMENT",
	"IPV4_TCP_SMALL_TTL",
	"IPV4_TCP_NEEDS_FRAGMENTATION",
	"IPV4_TCP_FLAGS",
	"IPV4_TCP_SEQ_EXCEEDS_RIGHT_EDGE",
	"IPV4_TCP_SMALL_DATA_OFFS",
	"IPV4_TCP_BAD_SACK",
	"IPV4_TCP_BIG_DATA_OFFS",
	"IPV4_TCP_SEQ_BEFORE_LEFT_EDGE",
	"IPV4_TCP_ACK_EXCEEDS_RIGHT_EDGE",
	"IPV4_TCP_ACK_BEFORE_LEFT_EDGE",
	"IPV4_UDP_NO_ICME",
	"IPV4_UDP_IP_OPTION",
	"IPV4_UDP_IP_FRAGMENT",
	"IPV4_UDP_SMALL_TTL",
	"IPV4_UDP_NEEDS_FRAGMENTATION",
	"IPV4_WRONG_TRAGET_MAC",
	"IPV4_IP_FRAGMENT",
	"IPV4_UNKNOWN_PROTOCOL"
};

static char *exception_events_ipv6_string[NA_EXCEPTION_EVENT_IPV6_LAST] = {
	"IPV6_ICMP_HEADER_NOT_FIT",
	"IPV6_ICMP_UNHANDLED_TYPE",
	"IPV6_ICMP_IP_NOT_FIT",
	"IPV6_ICMP_UDP_NOT_FIT",
	"IPV6_ICMP_TCP_NOT_FIT",
	"IPV6_ICMP_UNKNOWN_PROTOCOL",
	"IPV6_ICMP_NO_ICME",
	"IPV6_ICMP_FLUSH_TO_HOST",
	"IPV6_TCP_NO_ICME",
	"IPV6_TCP_SMALL_HOP_LIMIT",
	"IPV6_TCP_NEEDS_FRAGMENTATION",
	"IPV6_TCP_FLAGS",
	"IPV6_TCP_SEQ_EXCEEDS_RIGHT_EDGE",
	"IPV6_TCP_SMALL_DATA_OFFS",
	"IPV6_TCP_BAD_SACK",
	"IPV6_TCP_BIG_DATA_OFFS",
	"IPV6_TCP_SEQ_BEFORE_LEFT_EDGE",
	"IPV6_TCP_ACK_EXCEEDS_RIGHT_EDGE",
	"IPV6_TCP_ACK_BEFORE_LEFT_EDGE",
	"IPV6_UDP_NO_ICME",
	"IPV6_UDP_SMALL_HOP_LIMIT",
	"IPV6_UDP_NEEDS_FRAGMENTATION",
	"IPV6_WRONG_TRAGET_MAC",
	"IPV6_UNKNOWN_PROTOCOL"
};

/*
 * Structure that describes all TX metadata objects.
 */
struct na_tx_metadata_object {
	enum na_tx_metadata_types type;	/* Object type */
	union {				/* Sub-message type */
		struct na_ipv4_rule_create ipv4_rule_create;
		struct na_ipv4_rule_destroy ipv4_rule_destroy;
		struct na_ipv6_rule_create ipv6_rule_create;
		struct na_ipv6_rule_destroy ipv6_rule_destroy;
		struct na_mac_address_set mac_address_set;
	} sub;
};

/*
 * The NA IPv4 rule sync structure.
 */
struct na_ipv4_rule_sync {
	uint32_t index;			/* Slot id for cache stats to host OS */
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip;		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip;		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	int32_t flow_interface;		/* Flow interface number */
	uint32_t flow_max_window;	/* Flow direction's largest seen window */
	uint32_t flow_end;		/* Flow direction's largest seen sequence + segment length */
	uint32_t flow_max_end;		/* Flow direction's largest seen ack + max(1, win) */
	uint32_t flow_rx_packet_count;	/* Flow interface's RX packet count */
	uint32_t flow_rx_byte_count;	/* Flow interface's RX byte count */
	uint32_t flow_tx_packet_count;	/* Flow interface's TX packet count */
	uint32_t flow_tx_byte_count;	/* Flow interface's TX byte count */
	int32_t return_interface;	/* Return interface number */
	uint32_t return_max_window;	/* Return direction's largest seen window */
	uint32_t return_end;		/* Return direction's largest seen sequence + segment length */
	uint32_t return_max_end;	/* Return direction's largest seen ack + max(1, win) */
	uint32_t return_rx_packet_count;
					/* Return interface's RX packet count */
	uint32_t return_rx_byte_count;	/* Return interface's RX byte count */
	uint32_t return_tx_packet_count;
					/* Return interface's TX packet count */
	uint32_t return_tx_byte_count;	/* Return interface's TX byte count */
	uint32_t inc_ticks;		/* Number of ticks since the last sync */
	uint32_t generation;		/* Generation number for the cache entry */
};

/*
 * The NA IPv6 rule sync structure.
 */
struct na_ipv6_rule_sync {
	uint32_t index;			/* Slot id for cache stats to host OS */
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip[4];		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip[4];		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	int32_t flow_interface;		/* Flow interface number */
	uint32_t flow_max_window;	/* Flow direction's largest seen window */
	uint32_t flow_end;		/* Flow direction's largest seen sequence + segment length */
	uint32_t flow_max_end;		/* Flow direction's largest seen ack + max(1, win) */
	uint32_t flow_rx_packet_count;	/* Flow interface's RX packet count */
	uint32_t flow_rx_byte_count;	/* Flow interface's RX byte count */
	uint32_t flow_tx_packet_count;	/* Flow interface's TX packet count */
	uint32_t flow_tx_byte_count;	/* Flow interface's TX byte count */
	int32_t return_interface;	/* Return interface number */
	uint32_t return_max_window;	/* Return direction's largest seen window */
	uint32_t return_end;		/* Return direction's largest seen sequence + segment length */
	uint32_t return_max_end;	/* Return direction's largest seen ack + max(1, win) */
	uint32_t return_rx_packet_count;
					/* Return interface's RX packet count */
	uint32_t return_rx_byte_count;	/* Return interface's RX byte count */
	uint32_t return_tx_packet_count;
					/* Return interface's TX packet count */
	uint32_t return_tx_byte_count;	/* Return interface's TX byte count */
	uint32_t inc_ticks;		/* Number of ticks since the last sync */
	uint32_t generation;		/* Generation number for the cache entry */
};

/*
 * The NA per-interface statistics sync structure.
 */
struct na_interface_stats_sync {
	int32_t interface;		/* Interface number */
	uint32_t rx_packets;		/* Number of packets received */
	uint32_t rx_bytes;		/* Number of bytes received */
	uint32_t tx_packets;		/* Number of packets transmitted */
	uint32_t tx_bytes;		/* Number of bytes transmitted */
	uint32_t rx_errors;		/* Number of receive errors */
	uint32_t tx_errors;		/* Number of transmit errors */
	uint32_t tx_dropped;		/* Number of TX dropped packets */
	uint32_t collisions;		/* Number of TX and RX collisions */
	uint32_t host_rx_packets;	/* Number of RX packets received by host OS */
	uint32_t host_rx_bytes;		/* Number of RX bytes received by host OS */
	uint32_t host_tx_packets;	/* Number of TX packets sent by host OS */
	uint32_t host_tx_bytes;		/* Number of TX bytes sent by host OS */
	uint32_t rx_length_errors;	/* Number of RX length errors */
	uint32_t rx_overflow_errors;	/* Number of RX overflow errors */
	uint32_t rx_crc_errors;		/* Number of RX CRC errors */
	uint32_t exception_events_unknown[NA_EXCEPTION_EVENT_UNKNOWN_LAST];
					/* Number of unknown protocol exception events */
	uint32_t exception_events_ipv4[NA_EXCEPTION_EVENT_IPV4_LAST];
					/* Number of IPv4 exception events */
	uint32_t exception_events_ipv6[NA_EXCEPTION_EVENT_IPV6_LAST];
					/* Number of IPv6 exception events */
};

/*
 * The NA NA statistics sync structure.
 */
struct na_na_stats_sync {
	uint32_t ipv4_connection_create_requests;
					/* Number of IPv4 connection create requests */
	uint32_t ipv4_connection_create_collisions;
					/* Number of IPv4 connection create requests that collided with existing entries */
	uint32_t ipv4_connection_destroy_requests;
					/* Number of IPv4 connection destroy requests */
	uint32_t ipv4_connection_destroy_misses;
					/* Number of IPv4 connection destroy requests that missed the cache */
	uint32_t ipv4_connection_hash_hits;
					/* Number of IPv4 connection hash hits */
	uint32_t ipv4_connection_hash_reorders;
					/* Number of IPv4 connection hash reorders */
	uint32_t ipv4_connection_flushes;
					/* Number of IPv4 connection flushes */
	uint32_t ipv4_connection_evictions;
					/* Number of IPv4 connection evictions */
	uint32_t ipv6_connection_create_requests;
					/* Number of IPv6 connection create requests */
	uint32_t ipv6_connection_create_collisions;
					/* Number of IPv6 connection create requests that collided with existing entries */
	uint32_t ipv6_connection_destroy_requests;
					/* Number of IPv6 connection destroy requests */
	uint32_t ipv6_connection_destroy_misses;
					/* Number of IPv6 connection destroy requests that missed the cache */
	uint32_t ipv6_connection_hash_hits;
					/* Number of IPv6 connection hash hits */
	uint32_t ipv6_connection_hash_reorders;
					/* Number of IPv6 connection hash reorders */
	uint32_t ipv6_connection_flushes;
					/* Number of IPv6 connection flushes */
	uint32_t ipv6_connection_evictions;
					/* Number of IPv6 connection evictions */
};

/*
 * Types of RX metadata.
 */
enum na_rx_metadata_types {
	NA_RX_METADATA_TYPE_IPV4_RULE_SYNC,
	NA_RX_METADATA_TYPE_IPV6_RULE_SYNC,
	NA_RX_METADATA_TYPE_INTERFACE_STATS_SYNC,
	NA_RX_METADATA_TYPE_NA_STATS_SYNC
};

/*
 * Structure that describes all RX metadata objects.
 */
struct na_rx_metadata_object {
	enum na_rx_metadata_types type;	/* Object type */
	union {				/* Sub-message type */
		struct na_ipv4_rule_sync ipv4_rule_sync;
		struct na_ipv6_rule_sync ipv6_rule_sync;
		struct na_interface_stats_sync interface_stats_sync;
		struct na_na_stats_sync na_stats_sync;
	} sub;
};

#define NA_MAX_INTERFACES 2		/* Maximum number of interfaces we support */
#define NA_NUM_TX_DESCRIPTORS 128	/* Number of transmit descriptors */
#define NA_NUM_RX_DESCRIPTORS 128	/* Number of receive descriptors */
#define NA_NUM_PUSH_DESCRIPTORS 128	/* Number of push (new) buffer descriptors */
#define NA_NUM_POP_DESCRIPTORS 128	/* Number of pop (old) buffer descriptors */

/*
 * The NA per-interface fields of the NA memory map.
 */
struct na_if_map_interface {
	volatile uint32_t status;	/* Per-interface status words */
	volatile uint32_t control;	/* Control word */
};

/*
 * na_if_map status "register" bit definitions.
 */
#define NA_IF_MAP_INTERFACE_STATUS_LINK_UP 0x00000001
					/* Interface link is up */

/*
 * The NA descriptor structure.  This structure is common for both transmit and receive.
 */
struct na_desc {
	volatile uint8_t status;	/* Status */
	volatile int8_t interface_num;	/* Interface number */
	volatile uint16_t buffer_len;	/* Length of buffer */
	volatile uint32_t buffer;	/* Buffer address */
	volatile uint16_t payload_offs;	/* Offset from the start of the buffer to the payload */
	volatile uint16_t payload_len;	/* Offset from the start of the buffer to the payload */
	volatile uint32_t opaque;	/* Opaque reference used by host OS */
};

#define NA_DESC_STATUS_PKT 0x01		/* Descriptor is for packet data (0x01) or metadata (0x00) */
#define NA_DESC_STATUS_IPV4_IP_CHECKSUM_VALID 0x02
					/* Descriptor is for packet data where IPv4 header checksum is valid */
#define NA_DESC_STATUS_GEN_IPV4_IP_CHECKSUM 0x04
					/* Descriptor is for packet data where the IPv4 header checksum should be generated */
#define NA_DESC_STATUS_IP_TRANSPORT_CHECKSUM_VALID 0x08
					/* Descriptor is for packet data where the IP transport checksum is valid */
#define NA_DESC_STATUS_GEN_IP_TRANSPORT_CHECKSUM 0x10
					/* Descriptor is for packet data where the IP transport checksum should be generated */
#define NA_DESC_STATUS_FIRST_SEGMENT 0x20
					/* Descriptor contains the first segment of a frame */
#define NA_DESC_STATUS_LAST_SEGMENT 0x40
					/* Descriptor contains the last segment of a frame */
#define NA_DESC_STATUS_DISCARD 0x80	/* Descriptor is for a packet payload that should be discarded by the NA */

/*
 * Statistics/sysfs variables
 */
static uint8_t enable_statistics = 1;	/* Enables/disables statistics gathering. */
					/* 0 = disabled. Enabled by default */
static uint32_t cache_dev_major;	/* Major number of char device */

/*
 * Enumeration of the XML output.
 */
enum ubi32_na_stats_xml_states {
	UBI32_NA_STATS_XML_STATE_IPV4_START,
	UBI32_NA_STATS_XML_STATE_IPV4_STATS,
	UBI32_NA_STATS_XML_STATE_IPV4_CACHE_STATS,
	UBI32_NA_STATS_XML_STATE_IPV4_CACHE_START,
	UBI32_NA_STATS_XML_STATE_IPV4_CACHE_ENTRY,
	UBI32_NA_STATS_XML_STATE_IPV4_CACHE_END,
	UBI32_NA_STATS_XML_STATE_IPV4_END,
	UBI32_NA_STATS_XML_STATE_IPV6_START,
	UBI32_NA_STATS_XML_STATE_IPV6_STATS,
	UBI32_NA_STATS_XML_STATE_IPV6_CACHE_STATS,
	UBI32_NA_STATS_XML_STATE_IPV6_CACHE_START,
	UBI32_NA_STATS_XML_STATE_IPV6_CACHE_ENTRY,
	UBI32_NA_STATS_XML_STATE_IPV6_CACHE_END,
	UBI32_NA_STATS_XML_STATE_IPV6_END,
	UBI32_NA_STATS_XML_STATE_INTERFACES_START,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_START,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_STATS,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_HOST_STATS,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_EXCEPTION,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_START,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_STATS,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_EXCEPTION,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_END,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_START,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_STATS,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_EXCEPTION,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_END,
	UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_END,
	UBI32_NA_STATS_XML_STATE_INTERFACES_END,
	UBI32_NA_STATS_XML_STATE_DONE
};

/*
 * Static indexes for char device reads
 */
static uint8_t ubi32_na_sysfs_dev_open = 0;
static enum ubi32_na_stats_xml_states ubi32_na_stats_file_state = UBI32_NA_STATS_XML_STATE_IPV4_START;
static uint32_t ubi32_na_stats_file_index = 0;
static uint32_t ubi32_na_stats_file_index2 = 0;

/*
 * A big lock to protect sysfs and relevant funtions, variables
 */
spinlock_t sysfs_lock;

/*
 * SysFS class of the ubi32_na driver
 * SysFS control points can be found at /sys/devices/system/ubi32_na/ubi32_nax/...
 */
static struct sysdev_class ubi32_na_sysclass = {
	.name = "ubi32_na",
};

/*
 * SysFS linkage
 */
static struct sys_device ubi32_na_sys_dev;

/*
 * statistics struct
 */
#define IPV4_CONNECTION_ENTRIES 32
#define IPV6_CONNECTION_ENTRIES 32

/*
 * The NA memory map.  This is a set of pseudo registers and descriptor
 * arrays exposed to the host OS.
 */
struct na_if_map {
	/*
	 * Per-interface info.
	 */
	struct na_if_map_interface per_if[NA_MAX_INTERFACES];

	/*
	 * NA status info.
	 */
	volatile uint32_t int_status;	/* Summary interrupt status word */
	volatile uint32_t int_enable;	/* Interrupt enable mask */

	/*
	 * Descriptor indicies and arrays used for packet processing.
	 */
	volatile uint32_t push_host_index;
					/* Push descriptor index for the next packet buffer to be pushed (host view) */
	const volatile uint32_t push_na_index;
					/* Push descriptor index for the next packet buffer to be pushed (NA view) */
	volatile uint32_t tx_host_index;
					/* TX descriptor index for the next packet to be sent (host view) */
	const volatile uint32_t tx_na_index;
					/* TX descriptor index for the next packet to be send (NA view) */
	const volatile uint32_t rx_na_index;
					/* RX descriptor index for the next packet to be received (NA view) */
	volatile uint32_t rx_host_index;
					/* RX descriptor index for the next packet to be received (host view) */
	const volatile uint32_t pop_na_index;
					/* Pop descriptor index for the next packet buffer to be popped (NA view) */
	volatile uint32_t pop_host_index;
					/* Pop descriptor index for the next packet buffer to be popped (host view) */

	struct na_desc rx_desc[NA_NUM_RX_DESCRIPTORS];
	struct na_desc tx_desc[NA_NUM_TX_DESCRIPTORS];
	struct na_desc push_desc[NA_NUM_PUSH_DESCRIPTORS];
	struct na_desc pop_desc[NA_NUM_POP_DESCRIPTORS];

	/*
	 * Info used by the host OS.
	 */
	const uint32_t na_int;		/* Interrupt used by the host OS to signal the NA */
	const uint32_t host_int;	/* Interrupt used by the NA to signal the host OS */
	const uint32_t host_int_bit;	/* Interrupt bit that matches host_int */
};

/*
 * na_if_map int_status "register" bit definitions.
 */
#define NA_IF_MAP_INT_STATUS_RX 0x00000001
					/* RX packets available */
#define NA_IF_MAP_INT_STATUS_POP 0x00000002
					/* Packet buffers available to be popped */
#define NA_IF_MAP_INT_STATUS_PUSH 0x00000004
					/* Packet buffers need to be pushed */
#define NA_IF_MAP_INT_STATUS_TX 0x00000008
					/* TX slots available */

/*
 * na_if_map int_enable "register" bit definitions.
 */
#define NA_IF_MAP_INT_ENABLE_RX 0x00000001
					/* RX packets available */
#define NA_IF_MAP_INT_ENABLE_POP 0x00000002
					/* Packet buffers available to be popped */
#define NA_IF_MAP_INT_ENABLE_PUSH 0x00000004
					/* Packet buffers need to be pushed */
#define NA_IF_MAP_INT_ENABLE_TX 0x00000008
					/* TX slots available */

/*
 * na_if_map control "register" bit definitions.
 */
#define NA_IF_MAP_CONTROL_RX_ENABLE 0x00000001
					/* RX enabled */
#define NA_IF_MAP_CONTROL_TX_ENABLE 0x00000002
					/* TX enabled */

/*
 * NA device tree node definition.
 */
struct na_devtree_node {
	struct devtree_node dn;		/* Base class (must be first) */
	uint32_t num_interfaces;	/* Number of interfaces */
	struct na_if_map *map;		/* Pointer to the NA interface memory map */
};

#define UBI32_NA_VP_TX_TIMEOUT (10*HZ)

struct ubi32_na_private {
	struct net_device *dev;		/* Linux net device structure */
	uint32_t magic;			/* Used to confirm this private area is an NA private area */
	struct na_if_map_interface *nimi;
					/* Per-interface fields in the NA memory map */
	int32_t interface_num;		/* Interface number */

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	struct vlan_group *vlgrp;	/* VLAN acceleration info */
#endif
	struct rtnl_link_stats64 stats64;
					/* Host OS 64bit statistics structure */
	uint64_t host_rx_packets;	/* Number of RX packets received by host OS */
	uint64_t host_rx_bytes;		/* Number of RX bytes received by host OS */
	uint64_t host_tx_packets;	/* Number of TX packets sent by host OS */
	uint64_t host_tx_bytes;		/* Number of TX bytes sent by host OS */
	uint64_t ipv4_accelerated_rx_packets;
					/* Accelerated IPv4 RX packets */
	uint64_t ipv4_accelerated_rx_bytes;
					/* Accelerated IPv4 RX bytes */
	uint64_t ipv4_accelerated_tx_packets;
					/* Accelerated IPv4 TX packets */
	uint64_t ipv4_accelerated_tx_bytes;
					/* Accelerated IPv4 TX bytes */
	uint64_t ipv6_accelerated_rx_packets;
					/* Accelerated IPv6 RX packets */
	uint64_t ipv6_accelerated_rx_bytes;
					/* Accelerated IPv6 RX bytes */
	uint64_t ipv6_accelerated_tx_packets;
					/* Accelerated IPv6 TX packets */
	uint64_t ipv6_accelerated_tx_bytes;
					/* Accelerated IPv6 TX bytes */
	uint64_t exception_events_unknown[NA_EXCEPTION_EVENT_UNKNOWN_LAST];
					/* Unknown protocol exception events */
	uint64_t exception_events_ipv4[NA_EXCEPTION_EVENT_IPV4_LAST];
					/* IPv4 protocol exception events */
	uint64_t exception_events_ipv6[NA_EXCEPTION_EVENT_IPV6_LAST];
					/* IPv6 protocol exception events */
};

struct na_ipv4_statistics {
	uint32_t generation;		/* Generation number */
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip;		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip;		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	int32_t flow_interface;		/* Flow interface number */
	uint64_t flow_accelerated_rx_packets;
					/* Number of flow interface RX packets accelerated */
	uint64_t flow_accelerated_rx_bytes;
					/* Number of flow interface RX bytes accelerated */
	uint64_t flow_accelerated_tx_packets;
					/* Number of flow interface TX packets accelerated */
	uint64_t flow_accelerated_tx_bytes;
					/* Number of flow interface TX bytes accelerated */
	int32_t return_interface;	/* Return interface number */
	uint64_t return_accelerated_rx_packets;
					/* Number of return interface RX packets accelerated */
	uint64_t return_accelerated_rx_bytes;
					/* Number of return interface RX bytes accelerated */
	uint64_t return_accelerated_tx_packets;
					/* Number of return interface TX packets accelerated */
	uint64_t return_accelerated_tx_bytes;
					/* Number of return interface TX bytes accelerated */
	uint32_t last_sync;		/* Last sync time as jiffies */
};

struct na_ipv6_statistics {
	uint32_t generation;		/* Generation number */
	uint8_t protocol;		/* Protocol number */
	uint32_t src_ip[4];		/* Source IP address */
	uint32_t src_ident;		/* Source ident (e.g. port) */
	uint32_t dest_ip[4];		/* Destination IP address */
	uint32_t dest_ident;		/* Destination ident (e.g. port) */
	int32_t flow_interface;		/* Flow interface number */
	uint64_t flow_accelerated_rx_packets;
					/* Number of flow interface RX packets accelerated */
	uint64_t flow_accelerated_rx_bytes;
					/* Number of flow interface RX bytes accelerated */
	uint64_t flow_accelerated_tx_packets;
					/* Number of flow interface TX packets accelerated */
	uint64_t flow_accelerated_tx_bytes;
					/* Number of flow interface TX bytes accelerated */
	int32_t return_interface;	/* Return interface number */
	uint64_t return_accelerated_rx_packets;
					/* Number of return interface RX packets accelerated */
	uint64_t return_accelerated_rx_bytes;
					/* Number of return interface RX bytes accelerated */
	uint64_t return_accelerated_tx_packets;
					/* Number of return interface TX packets accelerated */
	uint64_t return_accelerated_tx_bytes;
					/* Number of return interface TX bytes accelerated */
	uint32_t last_sync;		/* Last sync time as jiffies */
};

struct ubi32_na_instance {
	struct napi_struct napi;
	spinlock_t lock;
	struct na_if_map *regs;
	uint32_t flags;			/* Bit flags */
	ubicom_na_ipv4_sync_callback_t ipv4_sync;
	ubicom_na_ipv6_sync_callback_t ipv6_sync;
	uint32_t rx_alloc_err;
	uint32_t tx_q_full_cnt;
	struct net_device *devices[NA_MAX_INTERFACES];
	uint64_t ipv4_accelerated_rx_packets;
					/* Accelerated IPv4 RX packets */
	uint64_t ipv4_accelerated_rx_bytes;
					/* Accelerated IPv4 RX bytes */
	uint64_t ipv4_accelerated_tx_packets;
					/* Accelerated IPv4 TX packets */
	uint64_t ipv4_accelerated_tx_bytes;
					/* Accelerated IPv4 TX bytes */
	uint64_t ipv4_connection_create_requests;
					/* Number of IPv4 connection create requests */
	uint64_t ipv4_connection_create_collisions;
					/* Number of IPv4 connection create requests that collided with existing entries */
	uint64_t ipv4_connection_destroy_requests;
					/* Number of IPv4 connection destroy requests */
	uint64_t ipv4_connection_destroy_misses;
					/* Number of IPv4 connection destroy requests that missed the cache */
	uint64_t ipv4_connection_hash_hits;
					/* Number of IPv4 connection hash hits */
	uint64_t ipv4_connection_hash_reorders;
					/* Number of IPv4 connection hash reorders */
	uint64_t ipv4_connection_flushes;
					/* Number of IPv4 connection flushes */
	uint64_t ipv4_connection_evictions;
					/* Number of IPv4 connection evictions */
	uint64_t ipv6_accelerated_rx_packets;
					/* Accelerated IPv6 RX packets */
	uint64_t ipv6_accelerated_rx_bytes;
					/* Accelerated IPv6 RX bytes */
	uint64_t ipv6_accelerated_tx_packets;
					/* Accelerated IPv6 TX packets */
	uint64_t ipv6_accelerated_tx_bytes;
					/* Accelerated IPv6 TX bytes */
	uint64_t ipv6_connection_create_requests;
					/* Number of IPv6 connection create requests */
	uint64_t ipv6_connection_create_collisions;
					/* Number of IPv6 connection create requests that collided with existing entries */
	uint64_t ipv6_connection_destroy_requests;
					/* Number of IPv6 connection destroy requests */
	uint64_t ipv6_connection_destroy_misses;
					/* Number of IPv6 connection destroy requests that missed the cache */
	uint64_t ipv6_connection_hash_hits;
					/* Number of IPv6 connection hash hits */
	uint64_t ipv6_connection_hash_reorders;
					/* Number of IPv6 connection hash reorders */
	uint64_t ipv6_connection_flushes;
					/* Number of IPv6 connection flushes */
	uint64_t ipv6_connection_evictions;
					/* Number of IPv6 connection evictions */
	struct na_ipv4_statistics na_ipv4_statistics[IPV4_CONNECTION_ENTRIES];
	struct na_ipv6_statistics na_ipv6_statistics[IPV6_CONNECTION_ENTRIES];
};

#define UBI32_NA_INSTANCE_FLAGS_TX_STOPPED 0x00000001
					/* Transmit queue was full so we stopped sending */

static struct ubi32_na_instance ubi32_na;

static u8_t na_mac_addr[NA_MAX_INTERFACES][ETH_ALEN] = {
	{0x00, 0x03, 0x64, 0xf3, 0x6c, 0x09},
	{0x00, 0x03, 0x64, 0xf3, 0x6c, 0x0a}
};

#define UBICOM_NA_NETDEV_MAGIC 0x1FEF5511		/* Special magic used to identify the private area of a net dev as being the NA */

static void ubi32_na_vp_rxtx_start(struct net_device *dev)
{
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;

	spin_lock_irqsave(&ubi32_na.lock, flags);
#if 0
	priv->regs->command = UBI32_NA_VP_CMD_RX_ENABLE | UBI32_NA_VP_CMD_TX_ENABLE;
#endif
	nim->int_enable = NA_IF_MAP_INT_ENABLE_RX | NA_IF_MAP_INT_ENABLE_POP |
				NA_IF_MAP_INT_ENABLE_PUSH | NA_IF_MAP_INT_ENABLE_TX;
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);
}

/*
 * We will use this code later.
 */
#if 0
static void ubi32_na_vp_rxtx_stop(struct net_device *dev)
{
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;

	spin_lock_irqsave(&ubi32_na.lock, flags);
#if 0
	nim->command = 0;
#endif
	nim->int_enable = 0;
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);

#if 0
	/* Wait for graceful shutdown */
	while (ubi32_na.regs->status & (UBI32_NA_VP_STATUS_RX_STATE | UBI32_NA_VP_STATUS_TX_STATE));
#endif
}
#endif

/*
 * ubi32_na_push()
 *	Push buffers to the NA.
 */
static void ubi32_na_push(struct na_if_map *nim)
{
	uint32_t push_count = NA_NUM_PUSH_DESCRIPTORS;

	/*
	 * Clear down any "push" interrupt flag.
	 */
	nim->int_status &= ~NA_IF_MAP_INT_STATUS_PUSH;

	/*
	 * Push a block of new buffers to the NA.
	 */
	while (push_count--) {
		uint32_t i = nim->push_host_index;
		uint32_t ni = (i + 1) & (NA_NUM_PUSH_DESCRIPTORS - 1);
		struct na_desc *pushdesc;
		struct sk_buff *skb;

		if (ni == nim->push_na_index) {
			break;
		}

		pushdesc = &nim->push_desc[i];
		skb = __dev_alloc_skb(2048 - 96, GFP_ATOMIC | __GFP_NOWARN);
		if (!skb) {
			ubi32_na.rx_alloc_err++;
			break;
		}

		pushdesc->opaque = (uint32_t)skb;
		pushdesc->buffer = virt_to_phys(skb->head);

		nim->push_host_index = ni;

		/*
		 * Now kick the NA awake.
		 */
		ubicom32_set_interrupt(nim->na_int);
	}
}

/*
 * ubi32_na_pop()
 *	Pop buffers from the NA.
 */
static void ubi32_na_pop(struct na_if_map *nim)
{
	/*
	 * Clear down any "pop" interrupt flag.
	 */
	nim->int_status &= ~NA_IF_MAP_INT_STATUS_POP;

	/*
	 * Pop a block of unused buffers from the NA.
	 */
	while (1) {
		uint32_t i = nim->pop_host_index;
		uint32_t ni;
		struct na_desc *popdesc;
		struct sk_buff *skb;

		if (i == nim->pop_na_index) {
			break;
		}

		popdesc = &nim->pop_desc[i];
		skb = (struct sk_buff *)popdesc->opaque;
		dev_kfree_skb_any(skb);

		ni = (i + 1) & (NA_NUM_POP_DESCRIPTORS - 1);
		nim->pop_host_index = ni;

		/*
		 * Now kick the NA awake.
		 */
		ubicom32_set_interrupt(nim->na_int);
	}
}

/*
 * ubi32_na_rx_metadata_ipv4_rule_sync()
 *	Handle the syncing of an IPv4 rule.
 */
static void ubi32_na_rx_metadata_ipv4_rule_sync(struct na_ipv4_rule_sync *nirs)
{
	struct ubicom_na_ipv4_sync unis;
	struct net_device *flow_dev = ubi32_na.devices[nirs->flow_interface];
	struct net_device *return_dev = ubi32_na.devices[nirs->return_interface];
	struct neighbour *neigh;

	ubicom_na_ipv4_sync_callback_t ipv4_sync = ubi32_na.ipv4_sync;
	if (!ipv4_sync) {
		return;
	}

	unis.protocol = nirs->protocol;
	unis.src_addr = nirs->src_ip;
	unis.src_port = nirs->src_ident;
	unis.dest_addr = nirs->dest_ip;
	unis.dest_port = nirs->dest_ident;
	unis.flow_max_window = nirs->flow_max_window;
	unis.flow_end = nirs->flow_end;
	unis.flow_max_end = nirs->flow_max_end;
	unis.flow_packet_count = nirs->flow_rx_packet_count;
	unis.flow_byte_count = nirs->flow_rx_byte_count;
	unis.return_max_window = nirs->return_max_window;
	unis.return_end = nirs->return_end;
	unis.return_max_end = nirs->return_max_end;
	unis.return_packet_count = nirs->return_rx_packet_count;
	unis.return_byte_count = nirs->return_rx_byte_count;

	/*
	 * Convert ms ticks from the NA to jiffies.  We know that inc_ticks is small
	 * and we expect HZ to be small too so we can multiply without worrying about
	 * wrap-around problems.  We add a rounding constant to ensure that the different
	 * time bases don't cause truncation errors.
	 */
	BUG_ON(HZ > 100000);
	unis.delta_jiffies = ((nirs->inc_ticks * HZ) + (MSEC_PER_SEC / 2)) / MSEC_PER_SEC;

	ipv4_sync(&unis);

	/*
	 * Update arp table
	 */
	if (!flow_dev) {
		return;
	}

	neigh = neigh_lookup(&arp_tbl, &nirs->src_ip, flow_dev);
	if (neigh) {
		neigh_update(neigh, NULL, neigh->nud_state, NEIGH_UPDATE_F_WEAK_OVERRIDE);
		neigh_release(neigh);
	}

	neigh = neigh_lookup(&arp_tbl, &nirs->dest_ip, flow_dev);
	if (neigh) {
		neigh_update(neigh, NULL, neigh->nud_state, NEIGH_UPDATE_F_WEAK_OVERRIDE);
		neigh_release(neigh);
	}

	if (enable_statistics) {
		struct ubi32_na_private *flow_priv;
		struct ubi32_na_private *return_priv;
		struct na_ipv4_statistics *nis;

		if (unlikely(nirs->index >= IPV4_CONNECTION_ENTRIES)) {
			printk(KERN_WARNING "Bad index: nirs->index:%d\n", nirs->index);
			return;
		}
		nis = &ubi32_na.na_ipv4_statistics[nirs->index];

		spin_lock_bh(&sysfs_lock);

		/*
		 * If our generation numbers don't match it means that this cache entry has
		 * been replaced since we last saw it and we need to reset things.
		 */
		if (unlikely(nis->generation != nirs->generation)) {
			nis->generation = nirs->generation;
			nis->protocol = nirs->protocol;
			nis->src_ip = nirs->src_ip;
			nis->src_ident = nirs->src_ident;
			nis->dest_ip = nirs->dest_ip;
			nis->dest_ident = nirs->dest_ident;
			nis->flow_interface = nirs->flow_interface;
			nis->flow_accelerated_rx_packets = 0;
			nis->flow_accelerated_rx_bytes = 0;
			nis->flow_accelerated_tx_packets = 0;
			nis->flow_accelerated_tx_bytes = 0;
			nis->return_interface = nirs->return_interface;
			nis->return_accelerated_rx_packets = 0;
			nis->return_accelerated_rx_bytes = 0;
			nis->return_accelerated_tx_packets = 0;
			nis->return_accelerated_tx_bytes = 0;
		}

		ubi32_na.ipv4_accelerated_rx_packets += nirs->flow_rx_packet_count + nirs->return_rx_packet_count;
		ubi32_na.ipv4_accelerated_rx_bytes += nirs->flow_rx_byte_count + nirs->return_rx_byte_count;
		ubi32_na.ipv4_accelerated_tx_packets += nirs->flow_tx_packet_count + nirs->return_tx_packet_count;
		ubi32_na.ipv4_accelerated_tx_bytes += nirs->flow_tx_byte_count + nirs->return_tx_packet_count;

		flow_priv = netdev_priv(flow_dev);
		flow_priv->ipv4_accelerated_rx_packets += nirs->flow_rx_packet_count;
		flow_priv->ipv4_accelerated_rx_bytes += nirs->flow_rx_byte_count;
		flow_priv->ipv4_accelerated_tx_packets += nirs->flow_tx_packet_count;
		flow_priv->ipv4_accelerated_tx_bytes += nirs->flow_tx_byte_count;

		return_priv = netdev_priv(return_dev);
		return_priv->ipv4_accelerated_rx_packets += nirs->return_rx_packet_count;
		return_priv->ipv4_accelerated_rx_bytes += nirs->return_rx_byte_count;
		return_priv->ipv4_accelerated_tx_packets += nirs->return_tx_packet_count;
		return_priv->ipv4_accelerated_tx_bytes += nirs->return_tx_byte_count;

		nis->flow_accelerated_rx_packets += nirs->flow_rx_packet_count;
		nis->flow_accelerated_rx_bytes += nirs->flow_rx_byte_count;
		nis->flow_accelerated_tx_packets += nirs->flow_tx_packet_count;
		nis->flow_accelerated_tx_bytes += nirs->flow_tx_byte_count;
		nis->return_accelerated_rx_packets += nirs->return_rx_packet_count;
		nis->return_accelerated_rx_bytes += nirs->return_rx_byte_count;
		nis->return_accelerated_tx_packets += nirs->return_tx_packet_count;
		nis->return_accelerated_tx_bytes += nirs->return_tx_byte_count;
		nis->last_sync = jiffies;
		spin_unlock_bh(&sysfs_lock);
	}
}

/*
 * ubi32_na_rx_metadata_ipv6_rule_sync()
 *	Handle the syncing of an IPv6 rule.
 */
static void ubi32_na_rx_metadata_ipv6_rule_sync(struct na_ipv6_rule_sync *nirs)
{
	struct ubicom_na_ipv6_sync unis;
	struct net_device *flow_dev = ubi32_na.devices[nirs->flow_interface];
	struct net_device *return_dev = ubi32_na.devices[nirs->return_interface];
	struct neighbour *neigh;

	ubicom_na_ipv6_sync_callback_t ipv6_sync = ubi32_na.ipv6_sync;
	if (!ipv6_sync) {
		return;
	}

	unis.protocol = nirs->protocol;
	unis.src_addr[0] = nirs->src_ip[0];
	unis.src_addr[1] = nirs->src_ip[1];
	unis.src_addr[2] = nirs->src_ip[2];
	unis.src_addr[3] = nirs->src_ip[3];
	unis.src_port = nirs->src_ident;
	unis.dest_addr[0] = nirs->dest_ip[0];
	unis.dest_addr[1] = nirs->dest_ip[1];
	unis.dest_addr[2] = nirs->dest_ip[2];
	unis.dest_addr[3] = nirs->dest_ip[3];
	unis.dest_port = nirs->dest_ident;
	unis.flow_max_window = nirs->flow_max_window;
	unis.flow_end = nirs->flow_end;
	unis.flow_max_end = nirs->flow_max_end;
	unis.flow_packet_count = nirs->flow_rx_packet_count;
	unis.flow_byte_count = nirs->flow_rx_byte_count;
	unis.return_max_window = nirs->return_max_window;
	unis.return_end = nirs->return_end;
	unis.return_max_end = nirs->return_max_end;
	unis.return_packet_count = nirs->return_rx_packet_count;
	unis.return_byte_count = nirs->return_rx_byte_count;

	/*
	 * Convert ms ticks from the NA to jiffies.  We know that inc_ticks is small
	 * and we expect HZ to be small too so we can multiply without worrying about
	 * wrap-around problems.  We add a rounding constant to ensure that the different
	 * time bases don't cause truncation errors.
	 */
	BUG_ON(HZ > 100000);
	unis.delta_jiffies = ((nirs->inc_ticks * HZ) + (MSEC_PER_SEC / 2)) / MSEC_PER_SEC;

	ipv6_sync(&unis);

	/*
	 * Update ndisc table
	 */
	if (!flow_dev) {
		return;
	}

	neigh = neigh_lookup(&nd_tbl, &nirs->src_ip, flow_dev);
	if (neigh) {
		neigh_update(neigh, NULL, neigh->nud_state, NEIGH_UPDATE_F_WEAK_OVERRIDE);
		neigh_release(neigh);
	}

	neigh = neigh_lookup(&nd_tbl, &nirs->dest_ip, flow_dev);
	if (neigh) {
		neigh_update(neigh, NULL, neigh->nud_state, NEIGH_UPDATE_F_WEAK_OVERRIDE);
		neigh_release(neigh);
	}

	if (enable_statistics) {
		struct ubi32_na_private *flow_priv;
		struct ubi32_na_private *return_priv;
		struct na_ipv6_statistics *nis;

		if (unlikely(nirs->index >= IPV6_CONNECTION_ENTRIES)) {
			printk(KERN_WARNING "Bad index: nirs->index:%d\n", nirs->index);
			return;
		}
		nis = &ubi32_na.na_ipv6_statistics[nirs->index];

		spin_lock_bh(&sysfs_lock);

		/*
		 * If our generation numbers don't match it means that this cache entry has
		 * been replaced since we last saw it and we need to reset things.
		 */
		if (unlikely(nis->generation != nirs->generation)) {
			nis->generation = nirs->generation;
			nis->protocol = nirs->protocol;
			nis->src_ip[0] = nirs->src_ip[0];
			nis->src_ip[1] = nirs->src_ip[1];
			nis->src_ip[2] = nirs->src_ip[2];
			nis->src_ip[3] = nirs->src_ip[3];
			nis->src_ident = nirs->src_ident;
			nis->dest_ip[0] = nirs->dest_ip[0];
			nis->dest_ip[1] = nirs->dest_ip[1];
			nis->dest_ip[2] = nirs->dest_ip[2];
			nis->dest_ip[3] = nirs->dest_ip[3];
			nis->dest_ident = nirs->dest_ident;
			nis->flow_interface = nirs->flow_interface;
			nis->flow_accelerated_rx_packets = 0;
			nis->flow_accelerated_rx_bytes = 0;
			nis->flow_accelerated_tx_packets = 0;
			nis->flow_accelerated_tx_bytes = 0;
			nis->return_interface = nirs->return_interface;
			nis->return_accelerated_rx_packets = 0;
			nis->return_accelerated_rx_bytes = 0;
			nis->return_accelerated_tx_packets = 0;
			nis->return_accelerated_tx_bytes = 0;
		}

		ubi32_na.ipv6_accelerated_rx_packets += nirs->flow_rx_packet_count + nirs->return_rx_packet_count;
		ubi32_na.ipv6_accelerated_rx_bytes += nirs->flow_rx_byte_count + nirs->return_rx_byte_count;
		ubi32_na.ipv6_accelerated_tx_packets += nirs->flow_tx_packet_count + nirs->return_tx_packet_count;
		ubi32_na.ipv6_accelerated_tx_bytes += nirs->flow_tx_byte_count + nirs->return_tx_packet_count;

		flow_priv = netdev_priv(flow_dev);
		flow_priv->ipv6_accelerated_rx_packets += nirs->flow_rx_packet_count;
		flow_priv->ipv6_accelerated_rx_bytes += nirs->flow_rx_byte_count;
		flow_priv->ipv6_accelerated_tx_packets += nirs->flow_tx_packet_count;
		flow_priv->ipv6_accelerated_tx_bytes += nirs->flow_tx_byte_count;

		return_priv = netdev_priv(return_dev);
		return_priv->ipv6_accelerated_rx_packets += nirs->return_rx_packet_count;
		return_priv->ipv6_accelerated_rx_bytes += nirs->return_rx_byte_count;
		return_priv->ipv6_accelerated_tx_packets += nirs->return_tx_packet_count;
		return_priv->ipv6_accelerated_tx_bytes += nirs->return_tx_byte_count;

		nis->flow_accelerated_rx_packets += nirs->flow_rx_packet_count;
		nis->flow_accelerated_rx_bytes += nirs->flow_rx_byte_count;
		nis->flow_accelerated_tx_packets += nirs->flow_tx_packet_count;
		nis->flow_accelerated_tx_bytes += nirs->flow_tx_byte_count;
		nis->return_accelerated_rx_packets += nirs->return_rx_packet_count;
		nis->return_accelerated_rx_bytes += nirs->return_rx_byte_count;
		nis->return_accelerated_tx_packets += nirs->return_tx_packet_count;
		nis->return_accelerated_tx_bytes += nirs->return_tx_byte_count;
		nis->last_sync = jiffies;
		spin_unlock_bh(&sysfs_lock);
	}
}

/*
 * ubi32_na_rx_metadata_interface_stats_sync()
 *	Handle the syncing of interface statistics.
 */
static void ubi32_na_rx_metadata_interface_stats_sync(struct na_interface_stats_sync *niss)
{
	struct net_device *dev;
	struct ubi32_na_private *priv;
	int i;

	dev = ubi32_na.devices[niss->interface];
	if (!dev) {
		return;
	}

	priv = netdev_priv(dev);
	priv->stats64.rx_packets += niss->rx_packets;
	priv->stats64.tx_packets += niss->tx_packets;
	priv->stats64.rx_bytes += niss->rx_bytes;
	priv->stats64.tx_bytes += niss->tx_bytes;
	priv->stats64.rx_errors += niss->rx_errors;
	priv->stats64.tx_errors += niss->tx_errors;
	priv->stats64.tx_dropped += niss->tx_dropped;
	priv->stats64.collisions += niss->collisions;
	priv->host_rx_packets += niss->host_rx_packets;
	priv->host_rx_bytes += niss->host_rx_bytes;
	priv->host_tx_packets += niss->host_tx_packets;
	priv->host_tx_bytes += niss->host_tx_bytes;
	priv->stats64.rx_length_errors += niss->rx_length_errors;
	priv->stats64.rx_over_errors += niss->rx_overflow_errors;
	priv->stats64.rx_crc_errors += niss->rx_crc_errors;

	/*
	 * If interface is bridged, update brige stats
	 */
	if (br_port_exists(dev)) {
		struct net_bridge_port *p;
		rcu_read_lock();
		p = br_port_get_rcu(dev);
		if (p) {
			struct net_bridge *br = p->br;
			if (br) {
				struct br_cpu_netstats *brstats = this_cpu_ptr(br->stats);
				u64_stats_update_begin(&brstats->syncp);
				brstats->rx_packets += niss->rx_packets;
				brstats->tx_packets += niss->tx_packets;
				brstats->rx_bytes += niss->rx_bytes;
				brstats->tx_bytes += niss->tx_bytes;
				u64_stats_update_end(&brstats->syncp);
			}
		}
		rcu_read_unlock();
	}

	for (i = 0; i < NA_EXCEPTION_EVENT_UNKNOWN_LAST; i++) {
		priv->exception_events_unknown[i] += niss->exception_events_unknown[i];
	}

	for (i = 0; i < NA_EXCEPTION_EVENT_IPV4_LAST; i++) {
		priv->exception_events_ipv4[i] += niss->exception_events_ipv4[i];
	}

	for (i = 0; i < NA_EXCEPTION_EVENT_IPV6_LAST; i++) {
		priv->exception_events_ipv6[i] += niss->exception_events_ipv6[i];
	}
}

/*
 * ubi32_na_rx_metadata_na_stats_sync()
 *	Handle the syncing of NA statistics.
 */
static void ubi32_na_rx_metadata_na_stats_sync(struct na_na_stats_sync *nnss)
{
	ubi32_na.ipv4_connection_create_requests += nnss->ipv4_connection_create_requests;
	ubi32_na.ipv4_connection_create_collisions += nnss->ipv4_connection_create_collisions;
	ubi32_na.ipv4_connection_destroy_requests += nnss->ipv4_connection_destroy_requests;
	ubi32_na.ipv4_connection_destroy_misses += nnss->ipv4_connection_destroy_misses;
	ubi32_na.ipv4_connection_hash_hits += nnss->ipv4_connection_hash_hits;
	ubi32_na.ipv4_connection_hash_reorders += nnss->ipv4_connection_hash_reorders;
	ubi32_na.ipv4_connection_flushes += nnss->ipv4_connection_flushes;
	ubi32_na.ipv4_connection_evictions += nnss->ipv4_connection_evictions;
	ubi32_na.ipv6_connection_create_requests += nnss->ipv6_connection_create_requests;
	ubi32_na.ipv6_connection_create_collisions += nnss->ipv6_connection_create_collisions;
	ubi32_na.ipv6_connection_destroy_requests += nnss->ipv6_connection_destroy_requests;
	ubi32_na.ipv6_connection_destroy_misses += nnss->ipv6_connection_destroy_misses;
	ubi32_na.ipv6_connection_hash_hits += nnss->ipv6_connection_hash_hits;
	ubi32_na.ipv6_connection_hash_reorders += nnss->ipv6_connection_hash_reorders;
	ubi32_na.ipv6_connection_flushes += nnss->ipv6_connection_flushes;
	ubi32_na.ipv6_connection_evictions += nnss->ipv6_connection_evictions;
}

/*
 * ubi32_na_rx_metadata()
 *	Handle metadata packets received from the NA.
 */
static void ubi32_na_rx_metadata(struct sk_buff *skb)
{
	uint32_t data_addr = (uint32_t)skb->data;
	struct na_rx_metadata_object *nrmo = (struct na_rx_metadata_object *)data_addr;

	if (skb->len < sizeof(struct na_rx_metadata_object)) {
		printk(KERN_WARNING "payload: %d bytes, struct: %ld bytes",
			skb->len, sizeof(struct na_rx_metadata_object));
		return;
	}

	switch (nrmo->type) {
	case NA_RX_METADATA_TYPE_IPV4_RULE_SYNC:
		ubi32_na_rx_metadata_ipv4_rule_sync(&nrmo->sub.ipv4_rule_sync);
		break;

	case NA_RX_METADATA_TYPE_IPV6_RULE_SYNC:
		ubi32_na_rx_metadata_ipv6_rule_sync(&nrmo->sub.ipv6_rule_sync);
		break;

	case NA_RX_METADATA_TYPE_INTERFACE_STATS_SYNC:
		ubi32_na_rx_metadata_interface_stats_sync(&nrmo->sub.interface_stats_sync);
		break;

	case NA_RX_METADATA_TYPE_NA_STATS_SYNC:
		ubi32_na_rx_metadata_na_stats_sync(&nrmo->sub.na_stats_sync);
		break;

	default:
		printk(KERN_WARNING "Unknown NRMO: %d", nrmo->type);
	}
}

/*
 * ubi32_na_rx()
 *	return number of frames processed
 */
static int ubi32_na_rx(struct na_if_map *nim, int quota)
{
	int count = 0;

	/*
	 * Clear down any RX interrupt flag.
	 */
	nim->int_status &= ~NA_IF_MAP_INT_STATUS_RX;

	/*
	 * Receive one or more RX packets from the NA.
	 */
	while (count < quota) {
		uint32_t i = nim->rx_host_index;
		uint32_t ni;
		struct na_desc *rxdesc;
		struct sk_buff *skb;
		struct net_device *dev;
		struct ubi32_na_private *priv;

		if (i == nim->rx_na_index) {
			break;
		}

		rxdesc = &nim->rx_desc[i];

		dev = ubi32_na.devices[rxdesc->interface_num];
		priv = netdev_priv(dev);

		skb = (struct sk_buff *)rxdesc->opaque;
		skb->data = skb->head + rxdesc->payload_offs;
		skb->len = rxdesc->payload_len;
		skb->tail = skb->data + skb->len;

		/*
		 * Is this packet data or metadata?
		 */
		if (unlikely(!(rxdesc->status & NA_DESC_STATUS_PKT))) {
			ubi32_na_rx_metadata(skb);
			dev_kfree_skb_any(skb);
			goto complete;
		}

		skb->dev = dev;

		/*
		 * By default we'll assume that the accelerator performed transport
		 * layer checksum verification but if it didn't we'll notify Linux
		 * that none happened and Linux can deal with this as it wants.
		 */
		skb->ip_summed = CHECKSUM_UNNECESSARY;
		if (unlikely(!(rxdesc->status & NA_DESC_STATUS_IP_TRANSPORT_CHECKSUM_VALID))) {
			skb->ip_summed = CHECKSUM_NONE;
		}

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
		/*
		 * If we're configured for VLAN processing then we need
		 * to strip the VLAN details and handle the packet via
		 * a VLAN receive function.
		 */
		if (priv->vlgrp && skb->data[12] == 0x81 && skb->data[13] == 0x00) {
			asm volatile (
			"	move.2	%0, 14(%1)	\n\t"
			"	move.2	14(%1), 10(%1)	\n\t"
			"	move.2	12(%1), 8(%1)	\n\t"
			"	move.2	10(%1), 6(%1)	\n\t"
			"	move.2	8(%1), 4(%1)	\n\t"
			"	move.2	6(%1), 2(%1)	\n\t"
			"	move.2	4(%1), 0(%1)	\n\t"
				: "=m" (skb->vlan_tci)
				: "a" (skb->data)
				: "memory"
			);

			skb_pull(skb, VLAN_HLEN);
			skb->protocol = eth_type_trans(skb, dev);
			vlan_hwaccel_receive_skb(skb, priv->vlgrp, skb->vlan_tci);
			goto complete;
		}
#endif

		skb->protocol = eth_type_trans(skb, dev);
		netif_receive_skb(skb);

	complete:
		/*
		 * Update the descriptor index info.
		 */
		ni = (i + 1) & (NA_NUM_RX_DESCRIPTORS - 1);
		nim->rx_host_index = ni;

		/*
		 * Now kick the NA awake.
		 */
		ubicom32_set_interrupt(nim->na_int);

		dev->last_rx = jiffies;
		count++;
	}

	return count;
}

/*
 * ubi32_na_napi_poll()
 *	polling routine used by NAPI
 */
static int ubi32_na_napi_poll(struct napi_struct *napi, int budget)
{
	u32_t count;

	/*
	 * First look to see if we're being asked for buffers to be pushed.
	 */
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t int_status = nim->int_status;
	if (unlikely(int_status & NA_IF_MAP_INT_STATUS_PUSH)) {
		ubi32_na_push(nim);
	}

	/*
	 * Do have any buffers to pop?
	 */
	if (unlikely(int_status & NA_IF_MAP_INT_STATUS_POP)) {
		ubi32_na_pop(nim);
	}

	/*
	 * If we have TX slots available check if we're currently stopped.
	 * If we are then restart things now.
	 */
	if (likely(int_status & NA_IF_MAP_INT_STATUS_TX)) {
		if (unlikely(ubi32_na.flags & UBI32_NA_INSTANCE_FLAGS_TX_STOPPED)) {
			unsigned int i;

printk(KERN_INFO "cleared TX stopped state\n");
			for (i = 0; i < NA_MAX_INTERFACES; i++) {
				struct net_device *ldev = ubi32_na.devices[i];
				if (ldev) {
					netif_wake_queue(ldev);
				}
			}
			ubi32_na.flags &= ~UBI32_NA_INSTANCE_FLAGS_TX_STOPPED;
		}
	}

	count = ubi32_na_rx(nim, budget);

	if (count < budget) {
		napi_complete(napi);
		nim->int_enable = NA_IF_MAP_INT_ENABLE_RX | NA_IF_MAP_INT_ENABLE_POP |
					NA_IF_MAP_INT_ENABLE_PUSH | NA_IF_MAP_INT_ENABLE_TX;
		if (nim->rx_na_index != nim->rx_host_index || nim->pop_na_index != nim->pop_host_index) {
			nim->int_enable = 0;
			napi_schedule(napi);
		}
	}

	return count;
}

/*
 * ubi32_na_link_state_poll()
 *	polling routine to check link state
 */
static inline void ubi32_na_link_state_poll(struct net_device *dev)
{
	struct ubi32_na_private *priv = netdev_priv(dev);
	struct na_if_map_interface *nimi = priv->nimi;

	if (likely(nimi->status & NA_IF_MAP_INTERFACE_STATUS_LINK_UP)) {
		if (unlikely(!netif_carrier_ok(dev))) {
			netif_carrier_on(dev);
		}
	} else if (netif_carrier_ok(dev)) {
		netif_carrier_off(dev);
	}
}

static irqreturn_t ubi32_na_interrupt(int irq, void *dev_id)
{
	struct na_if_map *nim = ubi32_na.regs;
	int i;

	for (i = 0; i < NA_MAX_INTERFACES; i++) {
		struct net_device *ldev = ubi32_na.devices[i];
		if (ldev) {
			ubi32_na_link_state_poll(ldev);
		}
	}

	if (unlikely(!(nim->int_status & nim->int_enable))) {
		return IRQ_NONE;
	}

	/*
	 * Disable new interrupts and start NAPI poll.
	 */
	nim->int_enable = 0;
	ubicom32_clear_interrupt(irq);
	napi_schedule(&ubi32_na.napi);

	return IRQ_HANDLED;
}

/*
 * ubi32_na_open
 */
static int ubi32_na_open(struct net_device *dev)
{
	struct ubi32_na_private *priv = netdev_priv(dev);
	struct na_if_map_interface *nimi = priv->nimi;

	printk(KERN_INFO "eth open %s, irq: %d\n", dev->name, dev->irq);

	/* check phy status and call netif_carrier_on */
	if (nimi->status & NA_IF_MAP_INTERFACE_STATUS_LINK_UP) {
		netif_carrier_on(dev);
	} else {
		netif_carrier_off(dev);
	}

	ubi32_na_vp_rxtx_start(dev);
	netif_start_queue(dev);
	return 0;
}

/*
 * We will use this code later.
 */
#if 0
/*
 * ubi32_na_destroy_all_rules
 *	Destroy all rules in NA.
 */
static int ubi32_na_destroy_all_rules(struct net_device *dev)
{
	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;

	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
		return 0;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'destroy all' message - marked as stopped\n");
		return 0;
	}

	ntmo.type = NA_TX_METADATA_TYPE_DESTROY_ALL_RULES;

	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);

	return 0;
}

static int ubi32_na_close(struct net_device *dev)
{
	struct ubi32_na_private *priv = netdev_priv(dev);
//	volatile void *pdata;
//	struct sk_buff *skb;

	printk(KERN_INFO "eth close %s\n", dev->name);
	netif_stop_queue(dev);	// can't transmit any more
	ubi32_na_vp_rxtx_stop(dev);

#if 0
	/*
	 * RX clean up
	 */
	while (priv->rx_tail != ubi32_na.regs->rx_in) {
		pdata = ubi32_na.regs->rx_dma_ring[priv->rx_tail];
		skb = container_of((void *)pdata, struct sk_buff, cb);
		ubi32_na.regs->rx_dma_ring[priv->rx_tail] = NULL;
		dev_kfree_skb_any(skb);
		priv->rx_tail = ((priv->rx_tail + 1) & RX_DMA_RING_MASK);
	}
	ubi32_na.regs->rx_in = 0;
	ubi32_na.regs->rx_out = ubi32_na.regs->rx_in;
	priv->rx_tail = ubi32_na.regs->rx_in;

	/*
	 * TX clean up
	 */
	BUG_ON(ubi32_na.regs->tx_out != ubi32_na.regs->tx_in);
	ubi32_na_tx_done(dev);
	BUG_ON(priv->tx_tail != ubi32_na.regs->tx_in);
	ubi32_na.regs->tx_in = 0;
	ubi32_na.regs->tx_out = ubi32_na.regs->tx_in;
	priv->tx_tail = ubi32_na.regs->tx_in;
#endif

	return 0;
}
#endif

/*
 * ubi32_na_set_config
 */
static int ubi32_na_set_config(struct net_device *dev, struct ifmap *map)
{
	/*
	 * if must to down to config it
	 */
	printk(KERN_INFO "set_config %x\n", dev->flags);
	if (dev->flags & IFF_UP)
		return -EBUSY;

	/*
	 * I/O and IRQ can not be changed
	 */
	if (map->base_addr != dev->base_addr) {
		printk(KERN_WARNING "%s: Can't change I/O address\n", dev->name);
		return -EOPNOTSUPP;
	}

	if (map->irq != dev->irq) {
		printk(KERN_WARNING "%s: Can't change IRQ\n", dev->name);
		return -EOPNOTSUPP;
	}

	/*
	 * ignore other fields
	 */
	return 0;
}

static int ubi32_na_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	unsigned long flags;
	struct ubi32_na_private *priv;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	uint32_t effective_na_index;
	struct na_desc *txdesc;
	uint32_t nr_frags;

	priv = netdev_priv(dev);
	spin_lock_irqsave(&ubi32_na.lock, flags);

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if (priv->vlgrp && vlan_tx_tag_present(skb)) {
		if (!skb_cow_head(skb, VLAN_HLEN)) {
			spin_unlock_irqrestore(&ubi32_na.lock, flags);
			return NETDEV_TX_BUSY;
		}

		skb_push(skb, VLAN_HLEN);
		asm volatile (
		"	btst	%0, #0		\n\t"
		"	jmpeq.t	1f		\n\t"

		"	move.1	0(%0), 4(%0)	\n\t"	// perform 8-bit op.
		"	move.1	1(%0), 5(%0)	\n\t"
		"	move.1	2(%0), 6(%0)	\n\t"
		"	move.1	3(%0), 7(%0)	\n\t"
		"	move.1	4(%0), 8(%0)	\n\t"
		"	move.1	5(%0), 9(%0)	\n\t"
		"	move.1	6(%0), 10(%0)	\n\t"
		"	move.1	7(%0), 11(%0)	\n\t"
		"	move.1	8(%0), 12(%0)	\n\t"
		"	move.1	9(%0), 13(%0)	\n\t"
		"	move.1	10(%0), 14(%0)	\n\t"
		"	move.1	11(%0), 15(%0)	\n\t"
		"	move.1	12(%0), #0x81	\n\t"
		"	move.1	13(%0), #0x00	\n\t"
		"	move.1	15(%0), %1	\n\t"
		"	lsr.4	%1, %1, #8	\n\t"
		"	move.1	14(%0), %1	\n\t"
		"	jmpt.t	2f		\n\t"

		"1:	move.2	0(%0), 4(%0)	\n\t"	// perform 16-bit op.
		"	move.2	2(%0), 6(%0)	\n\t"
		"	move.2	4(%0), 8(%0)	\n\t"
		"	move.2	6(%0), 10(%0)	\n\t"
		"	move.2	8(%0), 12(%0)	\n\t"
		"	move.2	10(%0), 14(%0)	\n\t"
		"	movei	12(%0),	#0x8100	\n\t"
		"	move.2	14(%0), %1	\n\t"

		"2:				\n\t"
			:
			: "a" (skb->data), "d" (skb->vlan_tci)
			: "cc", "memory"
		);
	}
#endif

#if 0
	/*
	 * Make skb->data private - our NA layer is going to want to modify the packet
	 * contents so we mustn't have any shared data.
	 */
	if (unlikely(skb_cloned(skb))) {
		struct sk_buff *skb_dup;
		skb_dup = skb_copy(skb, GFP_ATOMIC | __GFP_NOWARN);
		if (unlikely(!skb_dup)) {
			return NETDEV_TX_BUSY;
		}

		/*
		 * Free the old skb (reduce its reference count)
		 */
		dev_kfree_skb_any(skb);
		skb = skb_dup;
	}
#endif

	nr_frags = skb_shinfo(skb)->nr_frags;
	BUG_ON(nr_frags > MAX_SKB_FRAGS);
	BUG_ON(skb_shinfo(skb)->gso_size != 0);

	/*
	 * We need to work out if there's sufficent space in our transmit descriptor
	 * ring to place all of the segments of the skbuff.  As the ring wraps
	 * around we have to try to unwrap it to make meaningful comparisons.  We
	 * start by working out where our "unwrapped" next index will be after a
	 * successful completion.
	 */
	i = nim->tx_host_index;
	ni = i + 1 + nr_frags;

	/*
	 * Now we work out where our NA index is if we take an "unwrapped" view of
	 * it too.
	 *
	 * In reality the NA index is always behind the host index for the transmit
	 * descriptor ring but we're worried about the case where we wrap around and
	 * catch it up again so effectively we can think of it being ahead of us.
	 */
	effective_na_index = nim->tx_na_index;
	if (effective_na_index <= i) {
		effective_na_index += NA_NUM_TX_DESCRIPTORS;
	}

	if (ni >= effective_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		ubi32_na.flags |= UBI32_NA_INSTANCE_FLAGS_TX_STOPPED;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		netif_stop_queue(dev);
		printk(KERN_INFO "unable to enqueue 'tx' - marked as stopped\n");
		return NETDEV_TX_BUSY;
	}

	/*
	 * Is this a conventional unfragmented skbuff?
	 */
	if (likely(nr_frags == 0)) {
		/*
		 * We have a simple linear buffer.
		 */
		txdesc = &nim->tx_desc[i];
		txdesc->status = NA_DESC_STATUS_PKT | NA_DESC_STATUS_FIRST_SEGMENT | NA_DESC_STATUS_LAST_SEGMENT;
		if (likely(skb->ip_summed == CHECKSUM_PARTIAL)) {
			txdesc->status |= NA_DESC_STATUS_GEN_IP_TRANSPORT_CHECKSUM;
		}
		txdesc->interface_num = (int8_t)priv->interface_num;
		txdesc->opaque = (uint32_t)skb;
		txdesc->buffer = virt_to_phys(skb->head);
		txdesc->buffer_len = skb->end - skb->head;
		txdesc->payload_offs = skb->data - skb->head;
		txdesc->payload_len = skb->len;
	} else {
		uint32_t j = 0;
		skb_frag_t *frag;

		/*
		 * We have a multi-fragment buffer.  Handle the first fragment
		 * (the header of skb).
		 */
		txdesc = &nim->tx_desc[i];
		txdesc->status = NA_DESC_STATUS_PKT | NA_DESC_STATUS_FIRST_SEGMENT | NA_DESC_STATUS_DISCARD;
		if (likely(skb->ip_summed == CHECKSUM_PARTIAL)) {
			txdesc->status |= NA_DESC_STATUS_GEN_IP_TRANSPORT_CHECKSUM;
		}
		txdesc->interface_num = (int8_t)priv->interface_num;
		txdesc->opaque = (uint32_t)NULL;
		txdesc->buffer = virt_to_phys(skb->head);
		txdesc->buffer_len = skb->end - skb->head;
		txdesc->payload_offs = skb->data - skb->head;
		txdesc->payload_len = skb->len - skb->data_len;

		/*
		 * Now handle all fragments except the last one.
		 */
		while (unlikely(j < (nr_frags - 1))) {
			i = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);
			txdesc = &nim->tx_desc[i];

			frag = &skb_shinfo(skb)->frags[j++];
			txdesc->status = NA_DESC_STATUS_PKT | NA_DESC_STATUS_DISCARD;
			if (likely(skb->ip_summed == CHECKSUM_PARTIAL)) {
				txdesc->status |= NA_DESC_STATUS_GEN_IP_TRANSPORT_CHECKSUM;
			}
			txdesc->interface_num = (int8_t)priv->interface_num;
			txdesc->opaque = (uint32_t)NULL;
			txdesc->buffer = page_to_phys(frag->page) + frag->page_offset;
			txdesc->buffer_len = frag->size;
			txdesc->payload_offs = 0;
			txdesc->payload_len = frag->size;
		}

		/*
		 * Last fragment.
		 */
		i = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);
		txdesc = &nim->tx_desc[i];

		frag = &skb_shinfo(skb)->frags[j];
		txdesc->status = NA_DESC_STATUS_PKT | NA_DESC_STATUS_LAST_SEGMENT;
		if (likely(skb->ip_summed == CHECKSUM_PARTIAL)) {
			txdesc->status |= NA_DESC_STATUS_GEN_IP_TRANSPORT_CHECKSUM;
		}
		txdesc->interface_num = (int8_t)priv->interface_num;
		txdesc->opaque = (uint32_t)skb;
		txdesc->buffer = page_to_phys(frag->page) + frag->page_offset;
		txdesc->buffer_len = frag->size;
		txdesc->payload_offs = 0;
		txdesc->payload_len = frag->size;
	}

	/*
	 * Do an atomic update of the host transmit index.
	 */
// XXX - actually make this atomic!
	nim->tx_host_index = ni & (NA_NUM_TX_DESCRIPTORS - 1);

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);

	dev->trans_start = jiffies;
	return NETDEV_TX_OK;
}

/*
 * Deal with a transmit timeout.
 */
static void ubi32_na_tx_timeout(struct net_device *dev)
{
	struct na_if_map *nim = ubi32_na.regs;
	struct ubi32_na_private *priv = netdev_priv(dev);
	priv->stats64.tx_errors++;
	nim->int_enable |= NA_IF_MAP_INT_ENABLE_TX;
	ubicom32_set_interrupt(dev->irq);
	ubicom32_set_interrupt(nim->na_int);
}

static int ubi32_na_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
printk(KERN_WARNING "ubi32_na_ioctl");
#if 0
	struct ubi32_na_private *priv = netdev_priv(dev);
	struct mii_ioctl_data *data = if_mii(rq);

	printk(KERN_INFO "%s: ioctl %d\n", dev->name, cmd);
	switch (cmd) {
	case SIOCGMIIPHY:
		data->phy_id = 0;
		break;

	case SIOCGMIIREG:
		if ((data->reg_num & 0x1F) == MII_BMCR) {
			/*
			 * Make up MII control register value from what we know
			 */
			data->val_out = 0x0000
					| ((ubi32_na.regs->status & UBI32_NA_VP_STATUS_DUPLEX)
							? BMCR_FULLDPLX : 0)
					| ((ubi32_na.regs->status & UBI32_NA_VP_STATUS_SPEED100)
							? BMCR_SPEED100 : 0)
					| ((ubi32_na.regs->status & UBI32_NA_VP_STATUS_SPEED1000)
							? BMCR_SPEED1000 : 0);
		} else if ((data->reg_num & 0x1F) == MII_BMSR) {
			/*
			 *  Make up MII status register value from what we know
			 */
			data->val_out =
					(BMSR_100FULL|BMSR_100HALF|BMSR_10FULL|BMSR_10HALF)
					| ((ubi32_na.regs->status & NA_IF_MAP_INTERFACE_STATUS_LINK_UP)
							? BMSR_LSTATUS : 0);
		} else {
			return -EIO;
		}
		break;

	case SIOCSMIIREG:
		return -EOPNOTSUPP;
		break;

	default:
		return -EOPNOTSUPP;
	}
#endif

	return 0;
}

/*
 * Return statistics to the caller
 */
static struct rtnl_link_stats64 *ubi32_na_get_stats64(struct net_device *dev, struct rtnl_link_stats64 *s64)
{
	struct ubi32_na_private *priv = netdev_priv(dev);

	s64->rx_packets = priv->stats64.rx_packets;
	s64->rx_bytes = priv->stats64.rx_bytes;
	s64->rx_errors = priv->stats64.rx_errors;
	s64->rx_dropped = priv->stats64.rx_dropped;
	s64->tx_packets = priv->stats64.tx_packets;
	s64->tx_bytes = priv->stats64.tx_bytes;
	s64->tx_errors = priv->stats64.tx_errors;
	s64->tx_dropped = priv->stats64.tx_dropped;
	s64->multicast = priv->stats64.multicast;
	s64->collisions = priv->stats64.collisions;

	s64->rx_length_errors = priv->stats64.rx_length_errors;
	s64->rx_over_errors = priv->stats64.rx_over_errors;
	s64->rx_crc_errors = priv->stats64.rx_crc_errors;
	s64->rx_frame_errors = priv->stats64.rx_frame_errors;
	s64->rx_fifo_errors = priv->stats64.rx_fifo_errors;
	s64->rx_missed_errors = priv->stats64.rx_missed_errors;

	s64->tx_aborted_errors = priv->stats64.tx_aborted_errors;
	s64->tx_carrier_errors = priv->stats64.tx_carrier_errors;
	s64->tx_fifo_errors = priv->stats64.tx_fifo_errors;
	s64->tx_heartbeat_errors = priv->stats64.tx_heartbeat_errors;
	s64->tx_window_errors = priv->stats64.tx_window_errors;

	s64->rx_compressed = priv->stats64.rx_compressed;
	s64->tx_compressed = priv->stats64.tx_compressed;

	return s64;
}

static int ubi32_na_change_mtu(struct net_device *dev, int new_mtu)
{
	if ((new_mtu < 68) || (new_mtu > 1500))
		return -EINVAL;

	dev->mtu = new_mtu;
	printk(KERN_INFO "set mtu to %d", new_mtu);

	return 0;
}

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
/*
 * ubi32_na_vlan_rx_register: register vlan for RX acceleration
 */
static void ubi32_na_vlan_rx_register(struct net_device *dev, struct vlan_group *grp)
{
	struct ubi32_na_private *priv = netdev_priv(dev);

	printk(KERN_INFO "%s: register VLAN for HW acceleration\n", dev->name);
	priv->vlgrp = grp;
}
#endif

/*
 * ubi32_na_cleanup: unload the module
 */
void ubi32_na_cleanup(void)
{
	struct ubi32_na_private *priv;
	struct net_device *dev;
	int i;

	dev = ubi32_na.devices[0];
	if (dev) {
		napi_disable(&ubi32_na.napi);
		free_irq(dev->irq, dev);
		priv = netdev_priv(dev);
		unregister_netdev(dev);
		free_netdev(dev);
		ubi32_na.devices[0] = NULL;
	}

	for (i = 1; i < NA_MAX_INTERFACES; i++) {
		dev = ubi32_na.devices[i];
		if (dev) {
			priv = netdev_priv(dev);
			unregister_netdev(dev);
			free_netdev(dev);
			ubi32_na.devices[i] = NULL;
		}
	}
}

/*
 * ubi32_na_set_mac_addr
 *	Set MAC address of given net_device and also send it to NA.
 *
 * We set the given MAC address to net_device->dev_addr for Linux
 * and we also send it to NA for NA to update its local MAC address.
 * For this, we create a metadata which contains the MAC address and
 * the interface number (0 for eth0, 1 for eth1) and send it to NA.
 */
static int ubi32_na_set_mac_addr(struct net_device *dev, void *addr)
{
	struct sockaddr *mac = addr;

	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;
	struct na_mac_address_set *nmas = &ntmo.sub.mac_address_set;

	if (netif_running(dev))
		return -EBUSY;

	if (!is_valid_ether_addr(mac->sa_data))
		return -EADDRNOTAVAIL;

	/* Learn which interface this MAC address is to be set for. */
	/* HACK! FIX! */
	if (dev->name[3] == '0') {
		nmas->interface = 0;
	}
	else if (dev->name[3] == '1') {
		nmas->interface = 1;
	} else {
		printk(KERN_WARNING "Cannot set MAC address for device: %s\n", dev->name);
		return -EADDRNOTAVAIL;
	}

	/* Copy MAC address to na_mac_address_set structure */
	memcpy(nmas->mac_addr, mac->sa_data, ETH_ALEN);

	printk(KERN_INFO "MAC address to be set for interface %d: %x:%x:%x:%x:%x:%x\n", nmas->interface,
		nmas->mac_addr[0], nmas->mac_addr[1], nmas->mac_addr[2], nmas->mac_addr[3],
		nmas->mac_addr[4], nmas->mac_addr[5]);

	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
		return 0;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'set mac addr' - marked as stopped\n");
		return 0;
	}

	ntmo.type = NA_TX_METADATA_TYPE_MAC_ADDR_SET;

	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	/* Copy given MAC address to net_device`s dev_addr */
	memcpy(dev->dev_addr, nmas->mac_addr, ETH_ALEN);

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);

	return 0;
}

static const struct net_device_ops ubi32_netdev_ops = {
	.ndo_open = ubi32_na_open,
	//.ndo_stop = ubi32_na_close,
	.ndo_start_xmit = ubi32_na_start_xmit,
	.ndo_tx_timeout = ubi32_na_tx_timeout,
	.ndo_set_config = ubi32_na_set_config,
	.ndo_do_ioctl = ubi32_na_ioctl,
	.ndo_get_stats64 = ubi32_na_get_stats64,
	.ndo_change_mtu = ubi32_na_change_mtu,
	.ndo_set_mac_address = ubi32_na_set_mac_addr,
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	.ndo_vlan_rx_register = ubi32_na_vlan_rx_register,
#endif
};

static struct net_device *ubi32_na_alloc_netdev(int32_t interface_num, struct na_if_map *nim)
{
	struct net_device *dev;
	struct ubi32_na_private *priv;
	int i;

	dev = alloc_etherdev(sizeof(struct ubi32_na_private));
	if (!dev) {
		printk(KERN_WARNING "Failed to allocate net device object for interface: %d\n",
			interface_num);
		return NULL;
	}

	priv = netdev_priv(dev);
	priv->magic = UBICOM_NA_NETDEV_MAGIC;
	priv->dev = dev;
	priv->interface_num = interface_num;
	priv->nimi = &nim->per_if[interface_num];

	/*
	 * This just fill in some default Ubicom MAC address
	 */
	memcpy(dev->perm_addr, na_mac_addr[interface_num], ETH_ALEN);
	memcpy(dev->dev_addr, na_mac_addr[interface_num], ETH_ALEN);
	memset(dev->broadcast, 0xff, ETH_ALEN);

	/*
	 * Reset statistics info
	 */
	memset(&priv->stats64, 0, sizeof(struct rtnl_link_stats64));
	priv->host_rx_packets = 0;
	priv->host_rx_bytes = 0;
	priv->host_tx_packets = 0;
	priv->host_tx_bytes = 0;
	priv->ipv4_accelerated_rx_packets = 0;
	priv->ipv4_accelerated_rx_bytes = 0;
	priv->ipv4_accelerated_tx_packets = 0;
	priv->ipv4_accelerated_tx_bytes = 0;
	priv->ipv6_accelerated_rx_packets = 0;
	priv->ipv6_accelerated_rx_bytes = 0;
	priv->ipv6_accelerated_tx_packets = 0;
	priv->ipv6_accelerated_tx_bytes = 0;

	for (i = 0; i < NA_EXCEPTION_EVENT_UNKNOWN_LAST; i++) {
		priv->exception_events_unknown[i] = 0;
	}

	for (i = 0; i < NA_EXCEPTION_EVENT_IPV4_LAST; i++) {
		priv->exception_events_ipv4[i] = 0;
	}

	for (i = 0; i < NA_EXCEPTION_EVENT_IPV6_LAST; i++) {
		priv->exception_events_ipv6[i] = 0;
	}

	spin_lock_init(&ubi32_na.lock);

	dev->netdev_ops = &ubi32_netdev_ops;
	dev->watchdog_timeo = UBI32_NA_VP_TX_TIMEOUT;

	/*
	 * Interfaces 0 and 1 have support for checksum offloading.
	 */
	if (interface_num < 2) {
		dev->features |= NETIF_F_SG | NETIF_F_HW_CSUM;
		dev->vlan_features |= NETIF_F_SG | NETIF_F_HW_CSUM;
	}

	/*
	 * Enable VLAN acceleration
	 */
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	priv->vlgrp = NULL;
	dev->features |= NETIF_F_HW_VLAN_RX | NETIF_F_HW_VLAN_TX;
#endif

	return dev;
}

#define CHAR_DEV_MSG_SIZE 512

/*
 * ubi32_na_sysfs_char_device_read()
 *	Send cache entries info to userspace upon read request from user
 */
static ssize_t ubi32_na_sysfs_char_device_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	char msg[CHAR_DEV_MSG_SIZE];
	int bytes_read = 0;
	int total_read = 0;

	spin_lock_bh(&sysfs_lock);
	while ((ubi32_na_stats_file_state != UBI32_NA_STATS_XML_STATE_DONE) && (length > CHAR_DEV_MSG_SIZE)) {
		switch (ubi32_na_stats_file_state) {
		case UBI32_NA_STATS_XML_STATE_IPV4_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "<ipv4>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_STATS:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<stats "
					"rx_pkts=\"%llu\" rx_bytes=\"%llu\" "
					"tx_pkts=\"%llu\" tx_bytes=\"%llu\" />\n",
					ubi32_na.ipv4_accelerated_rx_packets,
					ubi32_na.ipv4_accelerated_rx_bytes,
					ubi32_na.ipv4_accelerated_tx_packets,
					ubi32_na.ipv4_accelerated_tx_bytes);
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_CACHE_STATS:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<cache_stats "
					"create_requests=\"%llu\" create_collisions=\"%llu\" "
					"destroy_requests=\"%llu\" destroy_misses=\"%llu\" "
					"flushes=\"%llu\" evictions=\"%llu\" "
					"hash_hits=\"%llu\" hash_reorders=\"%llu\" />\n",
					ubi32_na.ipv4_connection_create_requests,
					ubi32_na.ipv4_connection_create_collisions,
					ubi32_na.ipv4_connection_destroy_requests,
					ubi32_na.ipv4_connection_destroy_misses,
					ubi32_na.ipv4_connection_flushes,
					ubi32_na.ipv4_connection_evictions,
					ubi32_na.ipv4_connection_hash_hits,
					ubi32_na.ipv4_connection_hash_reorders);
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_CACHE_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<cache>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_CACHE_ENTRY:
			{
				struct na_ipv4_statistics *nis = &ubi32_na.na_ipv4_statistics[ubi32_na_stats_file_index];
				if (nis->last_sync) {
					/*
					 * current ipv4 cache etries in xml format
					 * <cache_ipv4 interface="" protocol="" src_ip="" src_ident="" dest_ip="" dest_ident=""
					 * sent_pkts="" recv_pkts="" sent_bytes="" recv_bytes="" last_sync="" \>
					 */
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t<entry "
							"protocol=\"%u\" src_ip=\"%pI4\" src_ident=\"%u\" "
							"dest_ip=\"%pI4\" dest_ident=\"%u\" "
							"flow_interface=\"%d\" "
							"flow_rx_pkts=\"%llu\" flow_rx_bytes=\"%llu\" "
							"flow_tx_pkts=\"%llu\" flow_tx_bytes=\"%llu\" "
							"ret_interface=\"%d\" "
							"ret_rx_pkts=\"%llu\" ret_rx_bytes=\"%llu\" "
							"ret_tx_pkts=\"%llu\" ret_tx_bytes=\"%llu\" />\n",
							nis->protocol,
							&nis->src_ip, nis->src_ident, &nis->dest_ip, nis->dest_ident,
							nis->flow_interface,
							nis->flow_accelerated_rx_packets,
							nis->flow_accelerated_rx_bytes,
							nis->flow_accelerated_tx_packets,
							nis->flow_accelerated_tx_bytes,
							nis->return_interface,
							nis->return_accelerated_rx_packets,
							nis->return_accelerated_rx_bytes,
							nis->return_accelerated_tx_packets,
							nis->return_accelerated_tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}
					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_index++;
				if (ubi32_na_stats_file_index >= IPV4_CONNECTION_ENTRIES) {
					ubi32_na_stats_file_index = 0;
					ubi32_na_stats_file_state++;
				}
			}
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_CACHE_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t</cache>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV4_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "</ipv4>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "<ipv6>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_STATS:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<stats "
					"rx_pkts=\"%llu\" rx_bytes=\"%llu\" "
					"tx_pkts=\"%llu\" tx_bytes=\"%llu\" />\n",
					ubi32_na.ipv6_accelerated_rx_packets,
					ubi32_na.ipv6_accelerated_rx_bytes,
					ubi32_na.ipv6_accelerated_tx_packets,
					ubi32_na.ipv6_accelerated_tx_bytes);
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_CACHE_STATS:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<cache_stats "
					"create_requests=\"%llu\" create_collisions=\"%llu\" "
					"destroy_requests=\"%llu\" destroy_misses=\"%llu\" "
					"flushes=\"%llu\" evictions=\"%llu\" "
					"hash_hits=\"%llu\" hash_reorders=\"%llu\" />\n",
					ubi32_na.ipv6_connection_create_requests,
					ubi32_na.ipv6_connection_create_collisions,
					ubi32_na.ipv6_connection_destroy_requests,
					ubi32_na.ipv6_connection_destroy_misses,
					ubi32_na.ipv6_connection_flushes,
					ubi32_na.ipv6_connection_evictions,
					ubi32_na.ipv6_connection_hash_hits,
					ubi32_na.ipv6_connection_hash_reorders);
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_CACHE_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<cache>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_CACHE_ENTRY:
			{
				struct na_ipv6_statistics *nis = &ubi32_na.na_ipv6_statistics[ubi32_na_stats_file_index];
				if (nis->last_sync) {
					/*
					 * current ipv6 cache etries in xml format
					 * <cache_ipv6 interface="" protocol="" src_ip="" src_ident="" dest_ip="" dest_ident=""
					 * sent_pkts="" recv_pkts="" sent_bytes="" recv_bytes="" last_sync="" \>
					 */
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t<entry "
							"protocol=\"%u\" src_ip=\"%pI6\" src_ident=\"%u\" "
							"dest_ip=\"%pI6\" dest_ident=\"%u\" "
							"flow_interface=\"%d\" "
							"flow_accel_rx_pkts=\"%llu\" flow_accel_rx_bytes=\"%llu\" "
							"flow_accel_tx_pkts=\"%llu\" flow_accel_tx_bytes=\"%llu\" "
							"ret_interface=\"%d\" "
							"ret_accel_rx_pkts=\"%llu\" ret_accel_rx_bytes=\"%llu\" "
							"ret_accel_tx_pkts=\"%llu\" ret_accel_tx_bytes=\"%llu\" />\n",
							nis->protocol,
							nis->src_ip, nis->src_ident, nis->dest_ip, nis->dest_ident,
							nis->flow_interface,
							nis->flow_accelerated_rx_packets,
							nis->flow_accelerated_rx_bytes,
							nis->flow_accelerated_tx_packets,
							nis->flow_accelerated_tx_bytes,
							nis->return_interface,
							nis->return_accelerated_rx_packets,
							nis->return_accelerated_rx_bytes,
							nis->return_accelerated_tx_packets,
							nis->return_accelerated_tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_index++;
				if (ubi32_na_stats_file_index >= IPV6_CONNECTION_ENTRIES) {
					ubi32_na_stats_file_index = 0;
					ubi32_na_stats_file_state++;
				}
			}
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_CACHE_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t</cache>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_IPV6_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "</ipv6>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "<interfaces>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_START:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					struct ubi32_na_private *priv = netdev_priv(dev);

					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t<interface num=\"%u\" name=\"%s\">\n",
								priv->interface_num, dev->name);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_STATS:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					struct ubi32_na_private *priv = netdev_priv(dev);
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
							"\t\t<stats rx_packets=\"%llu\" rx_bytes=\"%llu\" "
							"tx_packets=\"%llu\" tx_bytes=\"%llu\" />\n",
							priv->stats64.rx_packets, priv->stats64.rx_bytes,
							priv->stats64.tx_packets, priv->stats64.tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_HOST_STATS:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					struct ubi32_na_private *priv = netdev_priv(dev);
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
							"\t\t<host_stats rx_packets=\"%llu\" rx_bytes=\"%llu\" "
							"tx_packets=\"%llu\" tx_bytes=\"%llu\" />\n",
							priv->host_rx_packets, priv->host_rx_bytes,
							priv->host_tx_packets, priv->host_tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_EXCEPTION:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					uint64_t ct;
					struct ubi32_na_private *priv = netdev_priv(dev);
					ct = priv->exception_events_unknown[ubi32_na_stats_file_index2];
					if (ct) {
						bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
								"\t\t<exception name=\"%s\" count=\"%llu\" />\n",
								exception_events_unknown_string[ubi32_na_stats_file_index2],
								ct);
						if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
							continue;
						}

						length -= bytes_read;
						total_read += bytes_read;
					}
				}
			}

			ubi32_na_stats_file_index2++;
			if (ubi32_na_stats_file_index2 >= NA_EXCEPTION_EVENT_UNKNOWN_LAST) {
				ubi32_na_stats_file_index2 = 0;
				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t<ipv4>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_STATS:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					struct ubi32_na_private *priv = netdev_priv(dev);
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
							"\t\t\t<stats rx_packets=\"%llu\" rx_bytes=\"%llu\" "
							"tx_packets=\"%llu\" tx_bytes=\"%llu\" />\n",
							priv->ipv4_accelerated_rx_packets,
							priv->ipv4_accelerated_rx_bytes,
							priv->ipv4_accelerated_tx_packets,
							priv->ipv4_accelerated_tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_EXCEPTION:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					uint64_t ct;
					struct ubi32_na_private *priv = netdev_priv(dev);
					ct = priv->exception_events_ipv4[ubi32_na_stats_file_index2];
					if (ct) {
						bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
								"\t\t\t<exception name=\"%s\" count=\"%llu\" />\n",
								exception_events_ipv4_string[ubi32_na_stats_file_index2],
								ct);
						if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
							continue;
						}

						length -= bytes_read;
						total_read += bytes_read;
					}
				}
			}

			ubi32_na_stats_file_index2++;
			if (ubi32_na_stats_file_index2 >= NA_EXCEPTION_EVENT_IPV4_LAST) {
				ubi32_na_stats_file_index2 = 0;
				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV4_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t</ipv4>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_START:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t<ipv6>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_STATS:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					struct ubi32_na_private *priv = netdev_priv(dev);
					bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
							"\t\t\t<stats rx_packets=\"%llu\" rx_bytes=\"%llu\" "
							"tx_packets=\"%llu\" tx_bytes=\"%llu\" />\n",
							priv->ipv6_accelerated_rx_packets,
							priv->ipv6_accelerated_rx_bytes,
							priv->ipv6_accelerated_tx_packets,
							priv->ipv6_accelerated_tx_bytes);
					if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
						continue;
					}

					length -= bytes_read;
					total_read += bytes_read;
				}

				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_EXCEPTION:
			{
				struct net_device *dev = ubi32_na.devices[ubi32_na_stats_file_index];
				if (dev) {
					uint64_t ct;
					struct ubi32_na_private *priv = netdev_priv(dev);
					ct = priv->exception_events_ipv6[ubi32_na_stats_file_index2];
					if (ct) {
						bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE,
								"\t\t\t<exception name=\"%s\" count=\"%llu\" />\n",
								exception_events_ipv6_string[ubi32_na_stats_file_index2],
								ct);
						if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
							continue;
						}

						length -= bytes_read;
						total_read += bytes_read;
					}
				}
			}

			ubi32_na_stats_file_index2++;
			if (ubi32_na_stats_file_index2 >= NA_EXCEPTION_EVENT_IPV6_LAST) {
				ubi32_na_stats_file_index2 = 0;
				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_IPV6_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t\t</ipv6>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "\t</interface>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;

			ubi32_na_stats_file_index++;
			if (ubi32_na_stats_file_index < NA_MAX_INTERFACES) {
				ubi32_na_stats_file_state = UBI32_NA_STATS_XML_STATE_INTERFACES_INTERFACE_START;
			} else {
				ubi32_na_stats_file_index = 0;
				ubi32_na_stats_file_state++;
			}
			break;

		case UBI32_NA_STATS_XML_STATE_INTERFACES_END:
			bytes_read = snprintf(msg, CHAR_DEV_MSG_SIZE, "</interfaces>\n");
			if (copy_to_user(buffer + total_read, msg, CHAR_DEV_MSG_SIZE)) {
				continue;
			}

			length -= bytes_read;
			total_read += bytes_read;
			ubi32_na_stats_file_state++;
			break;

		case UBI32_NA_STATS_XML_STATE_DONE:
			break;
		}
	}

	spin_unlock_bh(&sysfs_lock);
	return total_read;
}

/*
 * ubi32_na_sysfs_char_device_write()
 *	Write to char device not required/supported
 */
static ssize_t ubi32_na_sysfs_char_device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	return -EINVAL;
}

/*
 * ubi32_na_sysfs_char_device_open()
 */
static int ubi32_na_sysfs_char_device_open(struct inode *inode, struct file *file)
{
	if (ubi32_na_sysfs_dev_open) {
		return -EBUSY;
	}
	ubi32_na_sysfs_dev_open++;
	ubi32_na_stats_file_state = UBI32_NA_STATS_XML_STATE_IPV4_START;
	ubi32_na_stats_file_index = 0;
	ubi32_na_stats_file_index2 = 0;

	return 0;
}

/*
 * ubi32_na_sysfs_char_device_release()
 */
static int ubi32_na_sysfs_char_device_release(struct inode *inode, struct file *file)
{
	ubi32_na_sysfs_dev_open--;
	return 0;
}

/*
 * File operations used in the sysfs char device
 */
static struct file_operations ubi32_na_sysfs_fops = {
	.read = ubi32_na_sysfs_char_device_read,
	.write = ubi32_na_sysfs_char_device_write,
	.open = ubi32_na_sysfs_char_device_open,
	.release = ubi32_na_sysfs_char_device_release
};

/*
 * ubi32_na_reset_counters()
 *	Reset sysfs statistic counters
 */
static void ubi32_na_reset_counters(void)
{
	int i;

	ubi32_na.ipv4_accelerated_rx_packets = 0;
	ubi32_na.ipv4_accelerated_rx_bytes = 0;
	ubi32_na.ipv4_accelerated_tx_packets = 0;
	ubi32_na.ipv4_accelerated_tx_bytes = 0;
	ubi32_na.ipv4_connection_create_requests = 0;
	ubi32_na.ipv4_connection_create_collisions = 0;
	ubi32_na.ipv4_connection_destroy_requests = 0;
	ubi32_na.ipv4_connection_destroy_misses = 0;
	ubi32_na.ipv4_connection_hash_hits = 0;
	ubi32_na.ipv4_connection_hash_reorders = 0;
	ubi32_na.ipv6_accelerated_rx_packets = 0;
	ubi32_na.ipv6_accelerated_rx_bytes = 0;
	ubi32_na.ipv6_accelerated_tx_bytes = 0;
	ubi32_na.ipv6_accelerated_tx_packets = 0;
	ubi32_na.ipv6_connection_create_requests = 0;
	ubi32_na.ipv6_connection_create_collisions = 0;
	ubi32_na.ipv6_connection_destroy_requests = 0;
	ubi32_na.ipv6_connection_destroy_misses = 0;
	ubi32_na.ipv6_connection_hash_hits = 0;
	ubi32_na.ipv6_connection_hash_reorders = 0;

	for (i = 0; i < IPV4_CONNECTION_ENTRIES; i++) {
		ubi32_na.na_ipv4_statistics[i].last_sync = 0;
	}

	for (i = 0; i < IPV6_CONNECTION_ENTRIES; i++) {
		ubi32_na.na_ipv6_statistics[i].last_sync = 0;
	}
}

/*
 * ubi32_na_get_enable_statistics()
 */
static ssize_t ubi32_na_get_enable_statistics(struct sys_device *dev, struct sysdev_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", enable_statistics);
}

/*
 * ubi32_na_set_enable_statistics()
 *	Get enable_statistics value from userspace, if it is 0, disable statistics support
 */
static ssize_t ubi32_na_set_enable_statistics(struct sys_device *dev, struct sysdev_attribute *attr, const char *buf, size_t count)
{
	spin_lock_bh(&sysfs_lock);
	enable_statistics = (uint8_t) simple_strtol(buf, NULL, 0);

	if (enable_statistics == 0) {
		ubi32_na_reset_counters();
	}
	spin_unlock_bh(&sysfs_lock);

	return count;
}

/*
 * ubi32_na_get_reset_counters()
 */
static ssize_t ubi32_na_get_reset_counters(struct sys_device *dev, struct sysdev_attribute *attr, char *buf)
{
	return sprintf(buf, "0\n");
}

/*
 * ubi32_na_set_reset_counters()
 *	Get reset_counters value, for any value, resets sysfs statistics counters
 */
static ssize_t ubi32_na_set_reset_counters(struct sys_device *dev, struct sysdev_attribute *attr, const char *buf, size_t count)
{
	spin_lock_bh(&sysfs_lock);
	ubi32_na_reset_counters();
	spin_unlock_bh(&sysfs_lock);
	return count;
}

/*
 * ubi32_na_get_cache_dev_major()
 *	Send major number of char device that hold cache entries data.
 */
static ssize_t ubi32_na_get_cache_dev_major(struct sys_device *dev, struct sysdev_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", cache_dev_major);
}

/*
 * Create sysfs attributes for sysfs entries
 */
static SYSDEV_ATTR(enable_statistics, 0644, ubi32_na_get_enable_statistics, ubi32_na_set_enable_statistics);
static SYSDEV_ATTR(reset_counters, 0644, ubi32_na_get_reset_counters, ubi32_na_set_reset_counters);
static SYSDEV_ATTR(cache_dev_major, 0444, ubi32_na_get_cache_dev_major, NULL);

/*
 * ubi32_na_add_sysfs()
 *	Add sysfs support, register sysfs class and create sysdev files. Register char device.
 */
int ubi32_na_add_sysfs(void)
{
	int result;

	spin_lock_init(&sysfs_lock);

	/*
	 * Register the sysfs class
	 */
	result = sysdev_class_register(&ubi32_na_sysclass);
	if (result) {
		printk(KERN_WARNING "Failed to register SysFS class %d\n", result);
		goto sysfs_cleanup_1;
	}

	/*
	 * Register SYSFS device control
	 */
	memset(&ubi32_na_sys_dev, 0, sizeof(ubi32_na_sys_dev));
	ubi32_na_sys_dev.id = 0;
	ubi32_na_sys_dev.cls = &ubi32_na_sysclass;
	result = sysdev_register(&ubi32_na_sys_dev);
	if (result) {
		printk(KERN_WARNING "Failed to register SysFS device %d\n", result);
		goto sysfs_cleanup_2;
	}

	/*
	 * Create files, one for each parameter supported by this module
	 */
	result = sysdev_create_file(&ubi32_na_sys_dev, &attr_enable_statistics);
	if (result) {
		printk(KERN_WARNING "Failed to register enable_statistics file %d\n", result);
		goto sysfs_cleanup_3;
	}

	result = sysdev_create_file(&ubi32_na_sys_dev, &attr_reset_counters);
	if (result) {
		printk(KERN_WARNING "Failed to register reset_counters file %d\n", result);
		goto sysfs_cleanup_8;
	}

	result = sysdev_create_file(&ubi32_na_sys_dev, &attr_cache_dev_major);
	if (result) {
		printk(KERN_WARNING "Failed to register cache_dev_major file %d\n", result);
		goto sysfs_cleanup_11;
	}

	/*
	 * Register a char device
	 */
	result = register_chrdev(0, ubi32_na_sysclass.name, &ubi32_na_sysfs_fops);
	if (result < 0) {
		printk(KERN_WARNING "Failed to register chrdev %d\n", result);
		goto sysfs_cleanup_12;
	}
	cache_dev_major = result;

	result = 0;
	return result;

sysfs_cleanup_12:
	sysdev_remove_file(&ubi32_na_sys_dev, &attr_cache_dev_major);
sysfs_cleanup_11:
	sysdev_remove_file(&ubi32_na_sys_dev, &attr_reset_counters);
sysfs_cleanup_8:
	sysdev_remove_file(&ubi32_na_sys_dev, &attr_enable_statistics);
sysfs_cleanup_3:
	sysdev_unregister(&ubi32_na_sys_dev);
sysfs_cleanup_2:
	sysdev_class_unregister(&ubi32_na_sysclass);
sysfs_cleanup_1:
	return result;
}

int ubi32_na_init_module(void)
{
	struct na_devtree_node *na_dn;
	struct net_device *dev;
	int i;
	int err;
	struct na_if_map *nim;

	/*
	 * Look for the device tree node.
	 */
	na_dn = (struct na_devtree_node *)devtree_find_node("NA");
	if (!na_dn) {
		printk(KERN_INFO "NA does not exist\n");
		return -ENODEV;
	}

	printk(KERN_INFO "Found NA - interfaces: %d\n", na_dn->num_interfaces);
	if (!na_dn->num_interfaces) {
		return -ENODEV;
	}

	nim = na_dn->map;
	ubi32_na.regs = nim;

	printk(KERN_INFO "NA: nim: 0x%p, na_int: %d host_int: %d\n",
		nim, nim->na_int, nim->host_int);

	/*
	 * Allocate the base net device.
	 */
	dev = ubi32_na_alloc_netdev(0, nim);
	if (!dev) {
		return -ENOMEM;
	}

	dev->irq = nim->host_int;

	err = register_netdev(dev);
	if (err) {
		printk(KERN_WARNING "Failed to register NA interface 0\n");
		free_netdev(dev);
		return -ENODEV;
	}

	ubi32_na.devices[0] = dev;
	netif_napi_add(dev, &ubi32_na.napi, ubi32_na_napi_poll, 64);

	err = request_irq(dev->irq, ubi32_na_interrupt, IRQF_DISABLED, dev->name, dev);
	if (err) {
		printk(KERN_WARNING "fail to request_irq %d\n", err);
		return -ENODEV;
	}

	napi_enable(&ubi32_na.napi);

	/*
	 * Device allocation.
	 */
	err = 0;
	for (i = 1; i < na_dn->num_interfaces; i++) {
		dev = ubi32_na_alloc_netdev(i, nim);
		if (!dev) {
			err = -ENOMEM;
			break;
		}

		err = register_netdev(dev);
		if (err) {
			printk(KERN_WARNING "Failed to register NA interface %d\n", i);
			free_netdev(dev);
			break;
		}

		ubi32_na.devices[i] = dev;
	}

	ubi32_na_add_sysfs();

	/*
	 * If we encountered any errors during initialization then tear down whatever
	 * we did complete and return an error.
	 */
	if (err) {
		ubi32_na_cleanup();
		return err;
	}

	return 0;
}

void ubicom_na_net_dev_register_ipv4_mgr(ubicom_na_ipv4_sync_callback_t event_callback)
{
	ubi32_na.ipv4_sync = event_callback;
}

void ubicom_na_net_dev_unregister_ipv4_mgr(void)
{
	ubi32_na.ipv4_sync = NULL;
}

/*
 * ubicom_na_create_ipv4_rule()
 *	Create a NA entry to accelerate the given connection
 */
void ubicom_na_create_ipv4_rule(struct ubicom_na_ipv4_create *unic)
{
	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;
	struct na_ipv4_rule_create *nirc = &ntmo.sub.ipv4_rule_create;

	printk(KERN_DEBUG "Create IPv4: %pI4:%d (%pI4:%d), %pI4:%d (%pI4:%d), p: %d\n",
		&unic->src_ip, unic->src_port, &unic->src_ip_xlate, unic->src_port_xlate,
		&unic->dest_ip, unic->dest_port, &unic->dest_ip_xlate, unic->dest_port_xlate, unic->protocol);
	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
		return;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'IPv4 rule create' - marked as stopped\n");
		return;
	}

	ntmo.type = NA_TX_METADATA_TYPE_IPV4_RULE_CREATE;
	nirc->src_interface_num = unic->src_interface_num;
	nirc->dest_interface_num = unic->dest_interface_num;
	nirc->protocol = (uint8_t)unic->protocol;
	nirc->src_ip = unic->src_ip;
	nirc->src_ident = (uint32_t)unic->src_port;
	nirc->dest_ip = unic->dest_ip;
	nirc->dest_ident = (uint32_t)unic->dest_port;
	nirc->src_ip_xlate = unic->src_ip_xlate;
	nirc->src_ident_xlate = (uint32_t)unic->src_port_xlate;
	nirc->dest_ip_xlate = unic->dest_ip_xlate;
	nirc->dest_ident_xlate = (uint32_t)unic->dest_port_xlate;
	memcpy(nirc->src_mac, unic->src_mac, 6);
	memcpy(nirc->dest_mac, unic->dest_mac, 6);
	memcpy(nirc->src_mac_xlate, unic->src_mac_xlate, 6);
	memcpy(nirc->dest_mac_xlate, unic->dest_mac_xlate, 6);
	nirc->flow_window_scale = unic->flow_window_scale;
	nirc->flow_max_window = unic->flow_max_window;
	nirc->flow_end = unic->flow_end;
	nirc->flow_max_end = unic->flow_max_end;
	nirc->return_window_scale = unic->return_window_scale;
	nirc->return_max_window = unic->return_max_window;
	nirc->return_end = unic->return_end;
	nirc->return_max_end = unic->return_max_end;
	nirc->flags = 0;
	if (unic->flags & UBICOM_NA_IPV4_CREATE_FLAG_NO_SEQ_CHECK) {
		nirc->flags |= NA_IPV4_RULE_CREATE_FLAG_NO_SEQ_CHECK;
	}
	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);
}

/*
 * ubicom_na_destroy_ipv4_rule()
 *	Destroy the given connection in the NA
 */
void ubicom_na_destroy_ipv4_rule(struct ubicom_na_ipv4_destroy *unid)
{
	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;
	struct na_ipv4_rule_destroy *nird = &ntmo.sub.ipv4_rule_destroy;

	printk(KERN_DEBUG "Destroy IPv4: %pI4:%d, %pI4:%d, p: %d\n",
		&unid->src_ip, unid->src_port, &unid->dest_ip, unid->dest_port, unid->protocol);
	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
// XXX - use a return code?
		return;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'IPv4 destroy rule' - marked as stopped\n");
		return;
	}

	ntmo.type = NA_TX_METADATA_TYPE_IPV4_RULE_DESTROY;
	nird->protocol = (uint8_t)unid->protocol;
	nird->src_ip = unid->src_ip;
	nird->src_ident = (uint32_t)unid->src_port;
	nird->dest_ip = unid->dest_ip;
	nird->dest_ident = (uint32_t)unid->dest_port;
	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	if (enable_statistics) {
		struct na_ipv4_statistics *nis;
		int i;

		spin_lock(&sysfs_lock);
		for (i = 0; i < IPV4_CONNECTION_ENTRIES; i++) {
			nis = &ubi32_na.na_ipv4_statistics[i];
			if ((nis->protocol == unid->protocol)
					&& (nis->src_ip == unid->src_ip)
					&& (nis->src_ident == unid->src_port)
					&& (nis->dest_ip == unid->dest_ip)
					&& (nis->dest_ident == unid->dest_port)) {
				nis->last_sync = 0;
				break;
			}
		}
		spin_unlock(&sysfs_lock);
	}

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);
}

/*
 * ubicom_na_net_dev_register_ipv6_mgr()
 *	Called to register an IPv6 connection manager with this driver
 */
void ubicom_na_net_dev_register_ipv6_mgr(ubicom_na_ipv6_sync_callback_t event_callback)
{
	ubi32_na.ipv6_sync = event_callback;
}

/*
 * ubicom_na_net_dev_unregister_ipv6_mgr()
 *	Called to unregister an IPv6 connection manager
 */
void ubicom_na_net_dev_unregister_ipv6_mgr(void)
{
	ubi32_na.ipv6_sync = NULL;
}

/*
 * ubicom_na_create_ipv6_rule()
 *	Create a NA entry to accelerate the given connection
 */
void ubicom_na_create_ipv6_rule(struct ubicom_na_ipv6_create *unic)
{
	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;
	struct na_ipv6_rule_create *nirc = &ntmo.sub.ipv6_rule_create;

	printk(KERN_DEBUG "Create IPv6: %pI6:%d, %pI6:%d, p: %d\n",
		unic->src_ip, unic->src_port, unic->dest_ip, unic->dest_port, unic->protocol);
	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
		return;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'IPv6 create rule' - marked as stopped\n");
		return;
	}

	ntmo.type = NA_TX_METADATA_TYPE_IPV6_RULE_CREATE;
	nirc->src_interface_num = unic->src_interface_num;
	nirc->dest_interface_num = unic->dest_interface_num;
	nirc->protocol = (uint8_t)unic->protocol;
	nirc->src_ip[0] = unic->src_ip[0];
	nirc->src_ip[1] = unic->src_ip[1];
	nirc->src_ip[2] = unic->src_ip[2];
	nirc->src_ip[3] = unic->src_ip[3];
	nirc->src_ident = (uint32_t)unic->src_port;
	nirc->dest_ip[0] = unic->dest_ip[0];
	nirc->dest_ip[1] = unic->dest_ip[1];
	nirc->dest_ip[2] = unic->dest_ip[2];
	nirc->dest_ip[3] = unic->dest_ip[3];
	nirc->dest_ident = (uint32_t)unic->dest_port;
	memcpy(nirc->src_mac, unic->src_mac, 6);
	memcpy(nirc->dest_mac, unic->dest_mac, 6);
	nirc->flow_window_scale = unic->flow_window_scale;
	nirc->flow_max_window = unic->flow_max_window;
	nirc->flow_end = unic->flow_end;
	nirc->flow_max_end = unic->flow_max_end;
	nirc->return_window_scale = unic->return_window_scale;
	nirc->return_max_window = unic->return_max_window;
	nirc->return_end = unic->return_end;
	nirc->return_max_end = unic->return_max_end;
	nirc->flags = 0;
	if (unic->flags & UBICOM_NA_IPV6_CREATE_FLAG_NO_SEQ_CHECK) {
		nirc->flags |= NA_IPV6_RULE_CREATE_FLAG_NO_SEQ_CHECK;
	}
	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);
}

/*
 * ubicom_na_destroy_ipv6_rule()
 *	Destroy the given connection in the NA
 */
void ubicom_na_destroy_ipv6_rule(struct ubicom_na_ipv6_destroy *unid)
{
	struct sk_buff *skb;
	unsigned long flags;
	struct na_if_map *nim = ubi32_na.regs;
	uint32_t i;
	uint32_t ni;
	struct na_desc *txdesc;
	struct na_tx_metadata_object ntmo;
	struct na_ipv6_rule_destroy *nird = &ntmo.sub.ipv6_rule_destroy;

	printk(KERN_DEBUG "Destroy IPv6: %pI6:%d, %pI6:%d, p: %d\n",
		unid->src_ip, unid->src_port, unid->dest_ip, unid->dest_port, unid->protocol);
	skb = __dev_alloc_skb(sizeof(struct na_tx_metadata_object), GFP_ATOMIC | __GFP_NOWARN);
	if (!skb) {
		ubi32_na.rx_alloc_err++;
// XXX - use a return code?
		return;
	}

	spin_lock_irqsave(&ubi32_na.lock, flags);
	i = nim->tx_host_index;
	ni = (i + 1) & (NA_NUM_TX_DESCRIPTORS - 1);

	if (ni == nim->tx_na_index) {
		/*
		 * We're not able to transmit right now!
		 */
		nim->int_status &= ~NA_IF_MAP_INT_STATUS_TX;
		ubi32_na.tx_q_full_cnt++;
		spin_unlock_irqrestore(&ubi32_na.lock, flags);
		dev_kfree_skb_any(skb);
		printk(KERN_INFO "unable to enqueue 'IPv6 destroy rule' - marked as stopped\n");
		return;
	}

	ntmo.type = NA_TX_METADATA_TYPE_IPV6_RULE_DESTROY;
	nird->protocol = (uint8_t)unid->protocol;
	nird->src_ip[0] = unid->src_ip[0];
	nird->src_ip[1] = unid->src_ip[1];
	nird->src_ip[2] = unid->src_ip[2];
	nird->src_ip[3] = unid->src_ip[3];
	nird->src_ident = (uint32_t)unid->src_port;
	nird->dest_ip[0] = unid->dest_ip[0];
	nird->dest_ip[1] = unid->dest_ip[1];
	nird->dest_ip[2] = unid->dest_ip[2];
	nird->dest_ip[3] = unid->dest_ip[3];
	nird->dest_ident = (uint32_t)unid->dest_port;
	memcpy(skb_put(skb, sizeof(struct na_tx_metadata_object)), &ntmo, sizeof(struct na_tx_metadata_object));

	txdesc = &nim->tx_desc[i];
	txdesc->status = 0;
	txdesc->interface_num = 0;
	txdesc->opaque = (uint32_t)skb;
	txdesc->buffer = virt_to_phys(skb->head);
	txdesc->buffer_len = skb->end - skb->head;
	txdesc->payload_offs = skb->data - skb->head;
	txdesc->payload_len = skb->len;

	nim->tx_host_index = ni;

	if (enable_statistics) {
		struct na_ipv6_statistics *nis;
		int i;

		spin_lock(&sysfs_lock);
		for (i = 0; i < IPV6_CONNECTION_ENTRIES; i++) {
			nis = &ubi32_na.na_ipv6_statistics[i];
			if ((nis->protocol == unid->protocol)
					&& (nis->src_ip[0] == unid->src_ip[0])
					&& (nis->src_ip[1] == unid->src_ip[1])
					&& (nis->src_ip[2] == unid->src_ip[2])
					&& (nis->src_ip[3] == unid->src_ip[3])
					&& (nis->src_ident == unid->src_port)
					&& (nis->dest_ip[0] == unid->dest_ip[0])
					&& (nis->dest_ip[1] == unid->dest_ip[1])
					&& (nis->dest_ip[2] == unid->dest_ip[2])
					&& (nis->dest_ip[3] == unid->dest_ip[3])
					&& (nis->dest_ident == unid->dest_port)) {
				nis->last_sync = 0;
				break;
			}
		}
		spin_unlock(&sysfs_lock);
	}

	/*
	 * Kick the NA.
	 */
	ubicom32_set_interrupt(nim->na_int);
	spin_unlock_irqrestore(&ubi32_na.lock, flags);
}

/*
 * ubicom_na_interface_number_get()
 *	Return the interface number of the NA net_device.
 *
 * NOTE: Returns non-zero on success, zero when the given dev is not an NA net_device.
 */
int32_t ubicom_na_interface_number_get(struct net_device *dev)
{
	struct ubi32_na_private *priv;
	priv = netdev_priv(dev);
	if (priv->magic != UBICOM_NA_NETDEV_MAGIC) {
		return -1;
	}
	return priv->interface_num;
}

module_init(ubi32_na_init_module);
module_exit(ubi32_na_cleanup);

EXPORT_SYMBOL(ubicom_na_net_dev_register_ipv4_mgr);
EXPORT_SYMBOL(ubicom_na_net_dev_unregister_ipv4_mgr);
EXPORT_SYMBOL(ubicom_na_net_dev_register_ipv6_mgr);
EXPORT_SYMBOL(ubicom_na_net_dev_unregister_ipv6_mgr);
EXPORT_SYMBOL(ubicom_na_create_ipv4_rule);
EXPORT_SYMBOL(ubicom_na_destroy_ipv4_rule);
EXPORT_SYMBOL(ubicom_na_create_ipv6_rule);
EXPORT_SYMBOL(ubicom_na_destroy_ipv6_rule);
EXPORT_SYMBOL(ubicom_na_interface_number_get);

MODULE_AUTHOR("Ubicom, Inc.");
MODULE_LICENSE("GPL");

